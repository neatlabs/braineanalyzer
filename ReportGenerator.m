classdef ReportGenerator < handle
    properties
        title
        author = 'NEATLabs Report Generator';
        type
        reportFile
    end
    properties(GetAccess=protected)
        rpt
    end
    methods
        function obj = ReportGenerator(reportTitle,reportType,reportFile)
            if nargin < 3
                reportFile = [];
            end
            obj.title = reportTitle;
            obj.type = reportType;
            obj.reportFile = reportFile;
        end
        function open(obj)
            if isempty(obj.reportFile)
                obj.reportFile = strrep(obj.title,' ','_');
            end
            obj.rpt = mlreportgen.report.Report(obj.reportFile,obj.type);
            titlepg = mlreportgen.report.TitlePage;
            titlepg.Title = obj.title;
            titlepg.Author = obj.author;
            add(obj.rpt,titlepg);
            add(obj.rpt,mlreportgen.report.TableOfContents);
        end
        function close(obj)
            close(obj.rpt);
        end
        function view(obj)
            rptview(obj.rpt);
        end
        function reportFile = generateReport(obj)
            error('A child class needs to implement this method!');
        end
        
        function [eventType, latency] = extractEvents(obj, EEG)
            error('A child class needs to implement the extractEvents method!');
        end
    end
    methods(Static)
        
        function [sec, EEGArray] = epoching(EEG, eventTypes, condition, timelim, sec)
            N = length(eventTypes);
            numberOfTrials = zeros(N,1);
            numberOfRejectedTrials = zeros(N,1);
            Condition = cell(1,N);
            for k=1:N
                EEGArray(k) = pop_epoch( EEG, eventTypes(k), timelim, 'epochinfo', 'yes');
                EEGArray(k).condition  = condition{k};
                Condition{k} = [upper(condition{k}(1)) lower(condition{k}(2:end))];
                numberOfTrials(k) = EEGArray(k).trials;
                [EEGArray(k), rejTrials] = pop_autorej(EEGArray(k), 'nogui','on','eegplot','off');
                numberOfRejectedTrials(k) = length(rejTrials);
                EEGArray(k) = pop_rmbase(EEGArray(k), [timelim(1)*1000 0]);
                EEGArray(k) = eeg_checkset(EEGArray(k));
            end
            D = cell(N,3);
            d = [numberOfTrials,numberOfRejectedTrials,100*numberOfRejectedTrials./numberOfTrials];
            for i=1:N, for j=1:3, D{i,j} = sprintf('%0.2f',d(i,j));end;end
            t = table(D(:,1),D(:,2),D(:,3));
            t.Properties.RowNames = Condition;
            t.Properties.VariableNames = {'Total','Rejected','Percentage'};
            t = mlreportgen.dom.Table(t);
            t.Style = {...
                mlreportgen.dom.RowSep('solid','black','1px'),...
                mlreportgen.dom.ColSep('solid','black','1px'),};
            t.Border = 'double';
            t.TableEntriesStyle = {mlreportgen.dom.HAlign('center')};
            add(sec, mlreportgen.dom.Paragraph('Artifacts rejection summary: '));
            add(sec,t);
        end
        
        function sec = topoplot(ERP, condition, latencies, chanlocs, times)
            latencies(latencies<min(times)) = min(times);
            latencies(latencies>max(times)) = max(times);
            
            % Topoplots
            N = size(ERP,3);
            n = length(latencies);            
            mx = inf;
            for k=1:N
                tmp = ERP(:,:,k);
                mx = min([mx prctile(abs(tmp(:)),97.5)]);
            end
            indLatency = interp1(times,1:length(times), latencies,'nearest');
            fig = figure('Position',[613,402,1040,370],'Color',[1 1 1],'Tag','BrainEAnalyzerFig');
            for k=1:N
                for i=1:n
                    ax = subplot(N,n,i+(k-1)*n);
                    topoplot(ERP(:,indLatency(i),k),chanlocs);
                    axis(ax,'equal','tight','on');
                    set(ax,'CLim',[-mx mx],'YTickLabel',[],'XTickLabel',[],'XColor',[1 1 1],'YColor',[1 1 1]);
                    if i==1
                        cond = [upper(condition{k}(1)) lower(condition{k}(2:end))]; 
                        ylabel(ax,cond);
                    end
                    if k==N
                        xlabel(ax,[num2str(latencies(i)) ' ms']);
                    end
                end
            end
            fig.Color = [1 1 1];
            colorbar('Position',[0.9331    0.6224    0.0130    0.2312]);
            colormap(bipolar(256,0.85));
            
            sec = mlreportgen.report.Section;
            sec.Title = 'ERP analysis';
            fig = mlreportgen.report.Figure(fig);
            fig.Snapshot.Height = '3in';
            fig.Snapshot.Width = '8in';
            fig.Snapshot.Caption = sprintf('ERP topographic summary.');
            add(sec,fig);
        end
            
        function sec = singleTrialAnalysis(EEGArray, targetCh, sec, sortingVal)
            N = length(EEGArray);
            if nargin < 4
                for k=1:N
                    sortingVal{k} = 1:EEGArray(k).trials;
                end
            end
            fig = figure('Tag','BrainEAnalyzerFig');
            if N>3
                fig.Position(3:4) = [1814 725];
            end
            mx = -inf;
            for k=1:N
                indCh = ismember({EEGArray(k).chanlocs.labels},targetCh);
                img = imgaussfilt(squeeze(EEGArray(k).data(indCh,:,:)),3);
                mx = max([mx prctile(abs(img(:)),97.5)*2]);
                [~, order{k}] = sort(sortingVal{k});
            end
            for k=1:N
                indCh = find(ismember({EEGArray(k).chanlocs.labels},targetCh));
                img = imgaussfilt(squeeze(EEGArray(k).data(indCh,:,order{k})),3);
                ax = subplot(2,N,k);
                imagesc(EEGArray(1).times,1:size(img,2), img')
                set(ax,'YDir','normal','XTickLabels',[],'CLim',[-mx mx]);
                hold(ax,'on');
                plot(ax,[0 0],ylim,'k-.');
                if k==1
                    ylabel(ax,'Trials')
                end
                condition  = [upper(EEGArray(k).condition(1)) lower(EEGArray(k).condition(2:end))];
                title(ax,[condition ' (' EEGArray(k).chanlocs(indCh).labels ')'])
                
                ax = subplot(2,N,k+N);
                plot(ax,EEGArray(k).times,mean(EEGArray(k).data(indCh,:,:),3))
                ax.XTick = EEGArray(k).times:250:EEGArray(k).times(end);
                ylim(ax,[-mx mx])
                hold(ax,'on');
                plot(ax,[0 0],ylim,'k-.');
                if k==1
                    ylabel(ax,['ERP (' '\muV)'])
                end
                xlabel(ax,'Time (ms)')
                grid(ax,'on');
            end
            colormap(bipolar(256,0.85))
            
            fig = mlreportgen.report.Figure(fig);
            if N<4
                fig.Snapshot.Width = '5in';
            else
                fig.Snapshot.Width = '8in';
            end
            fig.Snapshot.Caption = sprintf(['Channel ' targetCh ' single trial and ERP response.']);
            add(sec,fig);
        end
        
        function [ERSP, times, freq] = ersp(EEG, targetCh,baseline)
            n = length(targetCh);
            times = EEG.times;
            indrm = EEG.times<EEG.times(1)*0.9 | EEG.times>EEG.times(end)*0.9;
            times(indrm) = [];
            baseline(indrm) = [];
            for ch=1:n
                ind = ismember({EEG.chanlocs.labels},targetCh{ch});
                data = squeeze(EEG.data(ind,:,:));
                for t=1:EEG.trials    
                    [wt,freq] = cwt(data(:,t), 'amor', EEG.srate);
                    if ch==1 && t==1
                        Pxx = zeros([length(freq) length(times) EEG.trials]);
                        ERSP = zeros([length(freq) length(times) n]);
                        [~,sorting] = sort(freq);
                    end
                    Pxx(:,:,t) = abs(wt(sorting,~indrm)).^2;
                end
                mu = mean(mean(Pxx(:,baseline,:),2),3);
                ERSP(:,:,ch) = mean(bsxfun(@minus,Pxx, mu),3);
            end
            freq = freq(sorting);
            ERSP(freq<1,:,:) = [];
            freq(freq<1) = [];
        end
        
        function sec = erspImage(ERSP, condition, times, freq, channels, sec)
            %figure;
            %newtimef( squeeze(ERP(ind,:)),EEG_incongruent.pnts,[-500 1000],EEG_incongruent.srate, 0,'plotitc','off','baseline',[-500 0]);
            
            n = size(ERSP,3);
            mx = prctile((abs(ERSP(:))),95);
            cmap = bipolar(256,0.75);
            fig = figure('Tag','BrainEAnalyzerFig');
            fig.Position(3:4) = [971   367];
            indfreq = freq>2 & freq<40;
            for ch=1:n
                ax = subplot(1,n,ch);
                imagesc(ax,times,log10(freq(indfreq)),ERSP(indfreq,:,ch));
                % Fix the y-axis tick labels
                fval = 10.^ax.YTick;
                Nf = length(ax.YTick);
                yLabel = cell(Nf,1);
                fval(fval >= 10) = round(fval(fval >= 10));
                for it=1:Nf, yLabel{it} = num2str(fval(it),3);end
                set(ax,'YDir','normal','YTickLabel',yLabel,'CLim',[-mx mx]);
                hold(ax,'on');
                plot(ax,[0 0],ylim,'k-.');
                if ch==1
                    ylabel('Frequency (Hz)')
                end
                grid(ax,'on');
                xlabel('Time (ms)')
                title(['ERSP ' channels{ch} ' ' condition]);
                if ch==n
                    colorbar(ax,'Position',[0.9324    0.1090    0.0275    0.8147])
                end
            end
            colormap(cmap)
            fig = mlreportgen.report.Figure(fig);
            if n<4
                fig.Snapshot.Width = '5in';
            else
                fig.Snapshot.Width = '8in';
            end
            fig.Snapshot.Caption = sprintf(['Event-related spectral perturbation of ' condition '.']);
            add(sec,fig);
        end
        
        function latency = findPeaksLatency(ERP, times, baseline, postStm)
            if nargin < 3
                baseline = find(times >-100 & times <= 0);
            end
            if nargin < 4
                postStm = find(times >=100 & times <= 500);
            end
            mx = [];
            mn = [];
            N = size(ERP,1); 
            for ch=1:N
                mx = [mx findpeaks(ERP(ch,baseline))];
                [~,loc] = findpeaks(-ERP(ch,baseline));
                mn = [mn ERP(ch,baseline(loc))];
            end
            th_mn = prctile(mn,5);
            th_mx = prctile(mx,95);
            
            loc_mx = [];
            loc_mn = [];
            for ch=1:N
                [pk,loc] = findpeaks(ERP(ch,postStm));
                loc_mx = [loc_mx loc(pk > th_mx)];
                [~,loc] = findpeaks(-ERP(ch,postStm));
                pk = ERP(ch,postStm(loc));
                loc_mn = [loc_mn loc(pk < th_mn)];
            end
            loc_mx = unique(loc_mx);
            loc_mx(diff(loc_mx) < 5) = [];
            loc_mn = unique(loc_mn);
            loc_mn(diff(loc_mn) < 5) = [];
            latency = postStm(sort([loc_mx loc_mn]));
        end
        
        function sec = erpSourceAnalysis(ERP, hmfile, condition, latency, times)
            hm = headModel.loadFromFile(hmfile);
            n = length(latency);
            X = zeros(size(hm.K,2),n);
            solver = bsbl(hm);
            
            for k=1:n
                ind = latency(k)+(-5:5);
                x = solver.update(ERP(:,ind));
                X(:,k) = mean(abs(x),2);
            end
            
            cmap = bipolar(256,0.75);
            clim = [0 prctile(X(:),95)];
            fig = figure('Tag','BrainEAnalyzerFig');
            fig.Position(3:4) = [1363 356];
            for k=1:n
                ax = subplot(3,n,k);
                patch('vertices',hm.cortex.vertices,'Faces',hm.cortex.faces,'FaceVertexCData',X(:,k),...
                    'FaceColor','interp','FaceLighting','phong','LineStyle','none','FaceAlpha',0.3,'SpecularColorReflectance',0,...
                    'SpecularExponent',25,'SpecularStrength',0.25,'Parent',ax);
                set(ax,'Clim',clim,'XTick',[],'Ytick',[],'Ztick',[],'XColor',[1 1 1],'YColor',[1 1 1], 'ZColor',[1 1 1]);
                axis(ax, 'vis3d','equal','tight')
                view([-90 90])
                if k==1
                    xlabel(ax,'Dorsal');
                    title(ax,'L         R','fontweight','normal','fontsize',9);
                end
                if k==n
                    cb = colorbar(ax,'position', [0.9219 0.7093 0.0049 0.2157]);
                    cb.Ticks = cb.Ticks([1 end]);
                    cb.TickLabels = {'0','Max'};
                end
                
                ax = subplot(3,n,k+n);
                patch('vertices',hm.cortex.vertices,'Faces',hm.cortex.faces,'FaceVertexCData',X(:,k),...
                    'FaceColor','interp','FaceLighting','phong','LineStyle','none','FaceAlpha',0.3,'SpecularColorReflectance',0,...
                    'SpecularExponent',25,'SpecularStrength',0.25,'Parent',ax);
                set(ax,'Clim',clim,'XTick',[],'Ytick',[],'Ztick',[],'XColor',[1 1 1],'YColor',[1 1 1], 'ZColor',[1 1 1]);
                axis(ax, 'vis3d','equal','tight')
                view([0 0])
                if k==1
                    zlabel(ax,'Lateral');
                    title(ax,'P         A','fontweight','normal','fontsize',9);
                end
                
                ax = subplot(3,n,k+2*n);
                patch('vertices',hm.cortex.vertices,'Faces',hm.cortex.faces,'FaceVertexCData',X(:,k),...
                    'FaceColor','interp','FaceLighting','phong','LineStyle','none','FaceAlpha',0.3,'SpecularColorReflectance',0,...
                    'SpecularExponent',25,'SpecularStrength',0.25,'Parent',ax);
                set(ax,'Clim',clim,'XTick',[],'Ytick',[],'Ztick',[],'XColor',[1 1 1],'YColor',[1 1 1], 'ZColor',[1 1 1]);
                axis(ax, 'vis3d','equal','tight')
                view(ax,[-90 0]);
                if k==1
                    zlabel(ax,'Back');
                    title(ax,'L         R','fontweight','normal','fontsize',9);
                end
                ylabel(ax,[num2str(times(latency(k))) ' ms'])
            end
            colormap(cmap(end/2:end,:));
            if n<3
                fig.Position(3:4) = [428 356];
            end
                
            sec = mlreportgen.report.Section;
            sec.Title = ['ERP source analysis ' condition];
            fig = mlreportgen.report.Figure(fig);
            if n<3
                fig.Snapshot.Width = '2in';
            else
                fig.Snapshot.Width = '8in';
            end
            fig.Snapshot.Caption = sprintf(['ERP source power for ' condition '.']);
            add(sec,fig);
        end
        
        function sec = trialStats(EEGArray, targetCh)
            N = length(EEGArray);
            nch = length(targetCh);
            prcTrials = [0.1 0.15 0.25 0.5 0.6 0.7 0.8 0.9 1];
            x = cell(N,1);
            for i=1:N
                trialNumber = round(EEGArray(i).trials*prcTrials);
                nt = length(trialNumber);
                x{i} = zeros(nt, nch);
                erp = mean(EEGArray(i).data,3);
                for ch=1:nch
                    indCh = find(ismember({EEGArray(i).chanlocs.labels},targetCh{ch}));
                    for k=1:nt
                        x{i}(k,ch) = corr(erp(indCh,:)',mean(EEGArray(i).data(indCh,:,1:trialNumber(k)),3)');
                    end
                end
            end
            fig = figure('Position',[680   632   937   330],'Tag','BrainEAnalyzerFig');
            for i=1:N
                subplot(1,N,i);
                plot(prcTrials*100,x{i},'-.')
                hold on;
                h = plot(prcTrials*100,x{i},'o');
                set(h(1),'color',[0 0.447 0.741]);
                set(h(2),'color',[0.85, 0.325, 0.098]);
                xlabel('Fraction of trials (%)')
                if i==1
                    ylabel('Correlation');
                    legend(targetCh,'Location','southeast');
                end
                condition = [upper(EEGArray(i).condition(1)) lower(EEGArray(i).condition(2:end))];
                title(condition);
                grid on 
                ylim([0.5 1]);
                xlim(prcTrials([1 end])*100)
            end
            sec = mlreportgen.report.Section;
            sec.Title = 'Trial stats';
            fig = mlreportgen.report.Figure(fig);
            fig.Snapshot.Width = '5in';
            fig.Snapshot.Caption = sprintf('Correlation between the ERP and ERPs computed using a percentage of the total number of trials.');
            add(sec,fig);
        end
    end
end