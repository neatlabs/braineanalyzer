classdef LuckyDoor < brainePkg.BrainEAssessment

    methods
        function obj = LuckyDoor(latencies, channels, reportDir, doSourceAnalysis)
            obj = obj@brainePkg.BrainEAssessment('Lucky Door', latencies, channels, reportDir, doSourceAnalysis);
        end
        
        %% Generate report
        function [chap, EEG] = generateReport(obj, EEG)
            condition = {'positive','negative'};
            timelim = [-1 2];
            obj.latencies = -100:100:1800;
            
            [eventType, latency, positive, negative] = obj.extractEvents(EEG);
            EEG = eeg_addnewevents(EEG,latency, eventType);
            sec = mlreportgen.report.Section;
            sec.Title = 'Summary of feedback trials';
            [sec, EEGArray] = ReportGenerator.epoching(EEG, eventType, condition, timelim, sec);
            add(obj.chap, sec);
            
            ERP = zeros(EEG.nbchan, EEGArray(1).pnts,length(EEGArray));
            for k=1:length(EEGArray)
                ERP(:,:,k) = mean(EEGArray(k).data,3);
            end
            sec = brainePkg.LuckyDoor.topoplot(ERP, condition, obj.latencies, EEG.chanlocs, EEGArray(1).times);
            for ch=1:length(obj.channels)
                sec = ReportGenerator.singleTrialAnalysis(EEGArray, obj.channels{ch}, sec);
            end
            baseline = EEGArray(1).times>-1000 & EEGArray(1).times<=0;
            for k=1:length(EEGArray)
                [ERSP, times, freq] = ReportGenerator.ersp(EEGArray(k), obj.channels, baseline);
                sec = ReportGenerator.erspImage(ERSP, condition{k},times, freq, obj.channels, sec);
            end 
            add(obj.chap, sec);
           
            sec = ReportGenerator.trialStats(EEGArray, obj.channels);
            add(obj.chap, sec);
            chap = obj.chap;
        end
        
        function [eventType, latency, positive, negative] = extractEvents(obj, EEG)
            eventType = {EEG.event.type};
            eventLatency = cell2mat({EEG.event.latency});
            
            % Remove zero events
            indz = ismember(eventType,'0');
            eventType(indz) = [];
            eventLatency(indz) = [];
            for k=1:length(eventType)
                eventType{k} = str2double(eventType{k});
            end
            eventType = cell2mat(eventType);
            indnan = isnan(eventType);
            eventType(indnan) = [];
            eventLatency(indnan) = [];
            
            positive = strfind(eventType, 14);
            negative = strfind(eventType, 15);
            
            startEvent = find(ismember(eventType,800));
            endEvent   = find(ismember(eventType,801));
            
            positive    = positive(positive > startEvent(1) & positive < endEvent(2));
            negative    = negative(negative > startEvent(1) & negative < endEvent(2));
            
            latency = {eventLatency(positive), eventLatency(negative)};
            eventType = {'lucky_door_potitive', 'lucky_door_negative'};
        end
    end
    methods(Static)
        function sec = topoplot(ERP, condition, latencies, chanlocs, times)
            % Topoplots
            latLength = length(latencies);
            N = floor(latLength/10);
            n = ceil(latLength/N);
            mx = prctile(abs(ERP(:)),97.5);
                        
            indLatency = interp1(times,1:length(times), latencies,'nearest');
            indLatency = reshape(indLatency,n,2);
            latencies = reshape(latencies,n,2);
            
            fig = figure('Position',[613,402,1040,370],'Color',[1 1 1],'Tag','BrainEAnalyzerFig');
            panelCount = 1;
            for j=1:2
                for k=1:N
                    for i=1:n
                        
                        ax = subplot(N*2,n,panelCount);
                        topoplot(ERP(:,indLatency(i,k),j),chanlocs);
                        axis(ax,'equal','tight','on');
                        set(ax,'CLim',[-mx mx],'YTickLabel',[],'XTickLabel',[],'XColor',[1 1 1],'YColor',[1 1 1]);
                        if i==1
                            cond = [upper(condition{j}(1)) lower(condition{j}(2:end))];
                            ylabel(ax,cond);
                        end
                        xlabel(ax,[num2str(latencies(i,k)) ' ms']);
                        panelCount = panelCount+1;
                        if panelCount>latLength*2
                            break;
                        end
                    end
                end
            end
            fig.Color = [1 1 1];
            colorbar('Position',[0.9331    0.6224    0.0130    0.2312]);
            colormap(bipolar(256,0.85));
            
            sec = mlreportgen.report.Section;
            sec.Title = 'ERP analysis';
            fig = mlreportgen.report.Figure(fig);
            fig.Snapshot.Height = '3in';
            fig.Snapshot.Width = '8in';
            fig.Snapshot.Caption = sprintf('ERP topographic summary.');
            add(sec,fig);
        end
    end
end