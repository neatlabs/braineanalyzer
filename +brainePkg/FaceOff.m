classdef FaceOff < brainePkg.BrainEAssessment
    
    properties
        trialType = {'neutral', 'happy', 'angry', 'sad'};
    end
    
    methods
        function obj = FaceOff(latencies,channels, reportDir, doSourceAnalysis)
            obj = obj@brainePkg.BrainEAssessment('Face Off', latencies, channels, reportDir, doSourceAnalysis);
        end
        
        %% Generate report
        function [chap, EEG] = generateReport(obj, EEG)
            
            [eventType, latency, eventTypeArrow, latencyArrow] = obj.extractEvents(EEG);
            EEG = eeg_addnewevents(EEG,latency, eventType);
            EEG = eeg_addnewevents(EEG,latencyArrow, eventTypeArrow);
            sec = obj.summary(latency);
            
            timelim = [-1 1.5];
            [sec, EEGArray] = ReportGenerator.epoching(EEG, eventType, obj.trialType, timelim, sec);
            add(obj.chap, sec);
            
            ERP = zeros(EEG.nbchan, EEGArray(1).pnts,length(EEGArray));
            for k=1:length(EEGArray)
                ERP(:,:,k) = mean(EEGArray(k).data,3);
            end
            sec = ReportGenerator.topoplot(ERP, obj.trialType, obj.latencies, EEG.chanlocs, EEGArray(1).times);
            for ch=1:length(obj.channels)
                sec = ReportGenerator.singleTrialAnalysis(EEGArray, obj.channels{ch}, sec);
            end
            baseline = EEGArray(1).times>-1000 & EEGArray(1).times<=0;
            for k=1:length(EEGArray)
                [ERSP, times, freq] = ReportGenerator.ersp(EEGArray(k), obj.channels, baseline);
                sec = ReportGenerator.erspImage(ERSP, obj.trialType{k},times, freq, obj.channels, sec);
                add(obj.chap, sec);
            end
            
            
            sec = mlreportgen.report.Section;
            sec.Title = 'ERP analysis (centered on arrow)';
            timelim = [-0.5 0.5];
            [~, EEGArray] = ReportGenerator.epoching(EEG, eventTypeArrow, eventTypeArrow, timelim, sec);
            ERP = zeros(EEG.nbchan, EEGArray(1).pnts,length(EEGArray));
            for k=1:length(EEGArray)
                ERP(:,:,k) = mean(EEGArray(k).data,3);
            end
            sec = ReportGenerator.topoplot(ERP, eventTypeArrow, obj.latencies, EEG.chanlocs, EEGArray(1).times);
            for ch=1:length(obj.channels)
                sec = ReportGenerator.singleTrialAnalysis(EEGArray, obj.channels{ch}, sec);
            end
            baseline = EEGArray(1).times>-1000 & EEGArray(1).times<=0;
            for k=1:length(EEGArray)
                [ERSP, times, freq] = ReportGenerator.ersp(EEGArray(k), obj.channels, baseline);
                sec = ReportGenerator.erspImage(ERSP, eventTypeArrow{k},times, freq, obj.channels, sec);
                add(obj.chap, sec);
            end
            
            
            sec = ReportGenerator.trialStats(EEGArray, obj.channels);
            add(obj.chap, sec);
          
            chap = obj.chap;
        end
        
        function [eventType, latency,eventTypeArrow, latencyArrow] = extractEvents(obj,EEG)
            eventType = {EEG.event.type};
            eventLatency = cell2mat({EEG.event.latency});
            
            % Remove zero events
            indz = ismember(eventType,'0');
            eventType(indz) = [];
            eventLatency(indz) = [];
            for k=1:length(eventType)
                eventType{k} = str2double(eventType{k});
            end
            eventType = cell2mat(eventType);
            indnan = isnan(eventType);
            eventType(indnan) = [];
            eventLatency(indnan) = [];
            
            startEvent = find(ismember(eventType,400));
            endEvent   = find(ismember(eventType,401));
            startEvent = startEvent(2);
            endEvent   = endEvent(end);
            
            n = length(obj.trialType);
            latency = cell(1, n);
            latencyArrow = cell(1, n);
            for t=1:n
                markers = obj.getTrialMarkers(obj.trialType{t});
                eventInd = [];
                for i=1:2
                    for j=1:2
                        for k=1:4
                            eventInd = [eventInd strfind(eventType, markers(:,i,j,k)')];
                        end
                    end
                end    
                eventInd(eventInd < startEvent | eventInd > endEvent) = [];
                eventInd = sort(eventInd);
                latency{t} = eventLatency(eventInd+1);
                latencyArrow{t} = eventLatency(eventInd+2);
            end
            eventType = obj.trialType;
            eventTypeArrow = obj.trialType;
            for k=1:n
                eventTypeArrow{k} = ['a-' eventTypeArrow{k}];
            end
        end
        
        function markers = getTrialMarkers(obj, trialType)
            ind = strcmp(obj.trialType, trialType);
            if isempty(ind)
                error(['Trials of type ' trialType ' cannot be found.'])
            end
            markers = zeros([5,2,2,4]);
            if strcmp(obj.trialType{ind}, 'neutral')
                markers(:,1,1,1) = [10,1,21,12,14];
                markers(:,2,1,1) = [10,2,21,12,14];
                markers(:,1,2,1) = [10,1,22,13,14];
                markers(:,2,2,1) = [10,2,22,13,14];
                
                markers(:,1,1,2) = [10,1,23,12,14];
                markers(:,2,1,2) = [10,2,23,12,14];
                markers(:,1,2,2) = [10,1,24,13,14];
                markers(:,2,2,2) = [10,2,24,13,14];
                
                markers(:,1,1,3) = [10,1,21,13,15];
                markers(:,2,1,3) = [10,2,21,13,15];
                markers(:,1,2,3) = [10,1,22,12,15];
                markers(:,2,2,3) = [10,2,22,12,15];
                
                markers(:,1,1,4) = [10,1,23,13,15];
                markers(:,2,1,4) = [10,2,23,13,15];
                markers(:,1,2,4) = [10,1,24,13,15];
                markers(:,2,2,4) = [10,2,24,13,15];
                
            elseif strcmp(obj.trialType{ind}, 'happy')
                markers(:,1,1,1) = [10,3,21,12,14];
                markers(:,2,1,1) = [10,4,21,12,14];
                markers(:,1,2,1) = [10,3,22,13,14];
                markers(:,2,2,1) = [10,4,22,13,14];
                
                markers(:,1,1,2) = [10,3,23,12,14];
                markers(:,2,1,2) = [10,4,23,12,14];
                markers(:,1,2,2) = [10,3,24,13,14];
                markers(:,2,2,2) = [10,4,24,13,14];
                
                markers(:,1,1,3) = [10,3,21,13,15];
                markers(:,2,1,3) = [10,4,21,13,15];
                markers(:,1,2,3) = [10,3,22,12,15];
                markers(:,2,2,3) = [10,4,22,12,15];
                
                markers(:,1,1,4) = [10,3,23,13,15];
                markers(:,2,1,4) = [10,4,23,13,15];
                markers(:,1,2,4) = [10,3,24,13,15];
                markers(:,2,2,4) = [10,4,24,13,15];
            elseif strcmp(obj.trialType{ind}, 'angry')
                markers(:,1,1,1) = [10,5,21,12,14];
                markers(:,2,1,1) = [10,6,21,12,14];
                markers(:,1,2,1) = [10,5,22,13,14];
                markers(:,2,2,1) = [10,6,22,13,14];
                
                markers(:,1,1,2) = [10,5,23,12,14];
                markers(:,2,1,2) = [10,6,23,12,14];
                markers(:,1,2,2) = [10,5,24,13,14];
                markers(:,2,2,2) = [10,6,24,13,14];
                
                markers(:,1,1,3) = [10,5,21,13,15];
                markers(:,2,1,3) = [10,6,21,13,15];
                markers(:,1,2,3) = [10,5,22,12,15];
                markers(:,2,2,3) = [10,6,22,12,15];
                
                markers(:,1,1,4) = [10,5,23,13,15];
                markers(:,2,1,4) = [10,6,23,13,15];
                markers(:,1,2,4) = [10,5,24,13,15];
                markers(:,2,2,4) = [10,6,24,13,15];
                
            elseif strcmp(obj.trialType{ind}, 'sad')
                markers(:,1,1,1) = [10,7,21,12,14];
                markers(:,2,1,1) = [10,8,21,12,14];
                markers(:,1,2,1) = [10,7,22,13,14];
                markers(:,2,2,1) = [10,8,22,13,14];
                
                markers(:,1,1,2) = [10,7,23,12,14];
                markers(:,2,1,2) = [10,8,23,12,14];
                markers(:,1,2,2) = [10,7,24,13,14];
                markers(:,2,2,2) = [10,8,24,13,14];
                
                markers(:,1,1,3) = [10,7,21,13,15];
                markers(:,2,1,3) = [10,8,21,13,15];
                markers(:,1,2,3) = [10,7,22,12,15];
                markers(:,2,2,3) = [10,8,22,12,15];
                
                markers(:,1,1,4) = [10,7,23,13,15];
                markers(:,2,1,4) = [10,8,23,13,15];
                markers(:,1,2,4) = [10,7,24,13,15];
                markers(:,2,2,4) = [10,8,24,13,15];
            end
        end
    
        %% Summary
        function sec = summary(obj, eventLatency)
            sec = mlreportgen.report.Section;
            sec.Title = 'Summary of FaceOff trials';
            
            d = [length(eventLatency{1}) length(eventLatency{2}) length(eventLatency{3}), length(eventLatency{4})];
            t = table(d(1),d(2),d(3),d(4));
            t.Properties.RowNames = {'Total'};
            t.Properties.VariableNames = obj.upperTrialType();
            
            t = mlreportgen.dom.Table(t);
            t.Style = {...
                mlreportgen.dom.RowSep('solid','black','1px'),...
                mlreportgen.dom.ColSep('solid','black','1px'),};
            t.Border = 'double';
            t.TableEntriesStyle = {mlreportgen.dom.HAlign('center')};
            add(sec, mlreportgen.dom.Paragraph('Number of trials collected: '));
            add(sec,t);
        end
        
        %% Target vs Nontarget
        function [sec, EEG_array] = summaryERP(obj,EEG, latencies)
            sec = mlreportgen.report.Section;
            sec.Title = 'ERP analysis (emotional response to faces)';
            
            EEG_array = eeg_emptyset;
            Nem = length(obj.trialType);
            d = zeros(Nem,3);
            D = cell(Nem,3);
            for em=1:Nem
                EEG_array(em) = pop_epoch( EEG, obj.trialType(em), [-0.5  1], 'epochinfo', 'yes');
                nt = EEG_array(em).trials;
                [EEG_array(em), rej] = pop_autorej(EEG_array(em), 'nogui','on','eegplot','off');
                EEG_array(em) = pop_rmbase(EEG_array(em), [500 0], [1 126]);
                EEG_array(em) = eeg_checkset(EEG_array(em));
                
                d(em,:) = [nt length(rej) 100*length(rej)./nt];
                for j=1:3, D{em,j} = sprintf('%0.2f',d(em,j));end
            end
            t = table(D(:,1),D(:,2),D(:,3));
            t.Properties.RowNames = obj.upperTrialType();
            t.Properties.VariableNames = {'Total','Rejected','Percentage'};
            
            t = mlreportgen.dom.Table(t);
            t.Style = {...
                mlreportgen.dom.RowSep('solid','black','1px'),...
                mlreportgen.dom.ColSep('solid','black','1px'),};
            t.Border = 'double';
            t.TableEntriesStyle = {mlreportgen.dom.HAlign('center')};
            add(sec, mlreportgen.dom.Paragraph('Artifacts rejection summary: '));
            add(sec,t);
            mx = -inf;
            TrialType = obj.upperTrialType();
            for em=1:Nem
                tmp = mean(EEG_array(em).data,3);
                mx = max([mx prctile(abs(tmp(:)),97.5)*2]);
            end
            
            fig_topo = [];
            fig = figure('Position',[613,402,1040,509], 'Color',[1 1 1], 'Tag','BrainEAnalyzerFig');
            n = length(latencies);
            t = latencies;
            
            % Topoplots
            for em=1:Nem
                
                pop_topoplot(EEG_array(em),1,latencies,'',[1 length(latencies)]);
                fig_topo(end+1) = gcf;
                h = flipud(get(fig_topo(end),'Children'));
                figure(fig);
                for i=1:n
                    ax = subplot(Nem,n,i+(n*(em-1)));
                    copyobj(h(i).Children,ax);
                    axis equal tight
                    set(ax,'CLim',[-mx mx],'YTickLabel',[],'XTickLabel',[],'XColor',[1 1 1],'YColor',[1 1 1]);
                    if i==1
                        ylabel(ax,TrialType{em});
                    end
                    if em==Nem
                        xlabel(ax,[num2str(t(i)) ' ms']);
                    end
                end
            end
            colorbar(ax,'Position',[0.9337    0.7796    0.0124    0.1340]);
            colormap(bipolar(256,0.85))
            close(fig_topo);
            
            fig = mlreportgen.report.Figure(fig);
            fig.Snapshot.Height = '4in';
            fig.Snapshot.Width = '8in';
            fig.Snapshot.Caption = sprintf('ERP topographic summary for different emotions.');
            add(sec,fig);
        end
        
        function sec = erpImages(obj,sec, EEG_array, targetCh)
            indCh = find(ismember({EEG_array(1).chanlocs.labels},targetCh));
            Nem = length(obj.trialType);
            TrialType = obj.upperTrialType();
            mx = -inf;
            for em=1:Nem
                tmp = mean(EEG_array(em).data,3);
                mx = max([mx prctile(abs(tmp(:)),97.5)*2]);
            end
            fig = figure('Position',[613 484 1040 427], 'Tag','BrainEAnalyzerFig');
            for em=1:Nem
                
                img = imgaussfilt(squeeze(EEG_array(em).data(indCh,:,:)),3);
                ax = subplot(2,Nem,em);
                imagesc(img')
                set(ax,'YDir','normal','CLim',[-mx mx],'XTickLabels',[]);
                if em==1
                    ylabel('Trials')
                end
                title([TrialType{em} ' (' EEG_array(1).chanlocs(indCh).labels ')']);
                if em==Nem
                    colormap(bipolar(256,0.85))
                end
                
                ax = subplot(2,Nem,em+Nem);
                plot(EEG_array(1).times,mean(EEG_array(em).data(indCh,:,:),3))
                ax.XTick = EEG_array(1).times(1):250:EEG_array(1).times(end);
                ylim([-mx mx])
                xlim(EEG_array(1).times([1 end]))
                if em==1
                    ylabel(['ERP (' '\muV)'])
                end
                xlabel('Time (ms)')
                grid on;
            end
            colorbar(subplot(2,Nem,em),'Position',[0.9236    0.5841    0.0139    0.3405]);
            
            fig = mlreportgen.report.Figure(fig);
            fig.Snapshot.Height = '3in';
            fig.Snapshot.Width = '6in';
            fig.Snapshot.Caption = sprintf(['Channel ' targetCh ' ERP image for match and nonmatch conditions.']);
            add(sec,fig);
        end
        
        %% Trial stats
        function sec = trialStats(obj, EEG_array, targetCh)
            sec = mlreportgen.report.Section;
            sec.Title = 'Trial stats';

            Nem = length(obj.trialType);
            TrialType = obj.upperTrialType();
            
            nch = length(targetCh);
            prcTrials = [0.1 0.15 0.25 0.5 0.6 0.7 0.8 0.9 1];
            
            fig = figure('Position',[680   632   1040   330],'Tag','BrainEAnalyzerFig');
            
            for em=1:Nem
                trialNumber = round(EEG_array(em).trials*prcTrials);
                nt = length(trialNumber);
                x = zeros(nt, nch,Nem);
                erp = mean(EEG_array(em).data,3);
                for ch=1:nch
                    indCh = find(ismember({EEG_array(em).chanlocs.labels},targetCh{ch}));
                    for k=1:nt
                        x(k,ch,em) = corr(erp(indCh,:)',mean(EEG_array(em).data(indCh,:,1:trialNumber(k)),3)');
                    end
                end
                
                subplot(1,Nem,em);
                plot(prcTrials*100,x(:,:,em),'-.')
                hold on;
                h = plot(prcTrials*100,x(:,:,em),'o');
                set(h(1),'color',[0 0.447 0.741]);
                set(h(2),'color',[0.85, 0.325, 0.098]);
                xlabel('Fraction of trials (%)')
                if em==1
                    ylabel('Correlation');
                    legend(targetCh,'Location','southeast');
                end
                title(TrialType{em})
                grid on
                ylim([0.5 1]);
                xlim(prcTrials([1 end])*100)
            end
            fig = mlreportgen.report.Figure(fig);
            fig.Snapshot.Height = '2in';
            fig.Snapshot.Width = '8in';
            fig.Snapshot.Caption = sprintf('Correlation between the ERP and ERPs computed using a percentage of the total number of trials for different emotions.');
            add(sec,fig);
        end
        
        function TrialType = upperTrialType(obj)
            Nem = length(obj.trialType);
            TrialType = cell(1,Nem);
            for em=1:Nem
                TrialType{em} = [upper(obj.trialType{em}(1)) obj.trialType{em}(2:end)];
            end
        end
    end
end