classdef GoGreen < brainePkg.BrainEAssessment

    methods
        function obj = GoGreen(latencies, channels, reportDir, doSourceAnalysis)
            obj = obj@brainePkg.BrainEAssessment('Go Green', latencies, channels, reportDir, doSourceAnalysis);
        end
        
        %% Generate report
        function [chap, EEG] = generateReport(obj, EEG)
            condition = {'target1', 'nontarget1', 'target2', 'nontarget2'};
            timelim = [-1 1];
            
            [eventType, latency, targets_correct, targets_incorrect, nontargets_correct, nontargets_incorrect] = obj.extractEvents(EEG);
            EEG = eeg_addnewevents(EEG,latency, eventType);
            sec = brainePkg.GoGreen.summary(targets_correct, targets_incorrect, nontargets_correct, nontargets_incorrect);
            [sec, EEGArray] = ReportGenerator.epoching(EEG, eventType, condition, timelim, sec);
            add(obj.chap, sec);
            
            n = length(obj.channels);
            ERP = zeros(EEG.nbchan, EEGArray(1).pnts,n);
            ERP(:,:,1) = mean(EEGArray(1).data,3)-mean(EEGArray(3).data,3);     % targets 1 - targets 2
            ERP(:,:,2) = mean(EEGArray(2).data,3)-mean(EEGArray(4).data,3);     % nontargets 1 - nontargets 2
            
            condition = {'target1', 'nontarget1', 'target2', 'nontarget2', 'difference (T1-T2)', 'difference (NT1-NT2)'};
            sec = ReportGenerator.topoplot(ERP, condition, obj.latencies, EEG.chanlocs, EEGArray(1).times);
            
            for ch=1:n
                sec = ReportGenerator.singleTrialAnalysis(EEGArray, obj.channels{ch}, sec);
            end
            
            fig = figure('Position',[680 582 1148 380],'Color',[1 1 1],'Tag','BrainEAnalyzerFig');
            indCh = find(ismember({EEG.chanlocs.labels},obj.channels));
            tmp = ERP(indCh,:,:);
            mx = max(abs(tmp(:)))*1.1;
            for i=1:2
                ax = subplot(1,2,i);
                plot(ax,EEGArray(i).times, squeeze(ERP(indCh(i),:,:)));
                xlabel(ax,'Time (ms)')
                if i==1
                    ylabel(ax,'Difference wave (T1-T2)')
                    legend(ax,obj.channels)
                else
                    ylabel(ax,'Difference wave (NT1-NT2)')
                end
                grid(ax,'on');
                ylim(ax,[-mx mx]);
            end
            fig = mlreportgen.report.Figure(fig);
            fig.Snapshot.Height = '3in';
            fig.Snapshot.Width = '8in';
            fig.Snapshot.Caption = sprintf('Difference wave.');
            add(sec,fig);
            
            
            baseline = EEGArray(1).times>-500 & EEGArray(1).times<=0;
            [ERSP1, times, freq] = ReportGenerator.ersp(EEGArray(1), obj.channels, baseline);% targets 1 
            [ERSP2, times, freq] = ReportGenerator.ersp(EEGArray(2), obj.channels, baseline);% nontargets 1
            [ERSP3, times, freq] = ReportGenerator.ersp(EEGArray(3), obj.channels, baseline);% targets 2
            [ERSP4, times, freq] = ReportGenerator.ersp(EEGArray(4), obj.channels, baseline);% nontargets 2
            sec = ReportGenerator.erspImage(ERSP1-ERSP2, 'difference (T1-T2)',times, freq, obj.channels, sec);
            sec = ReportGenerator.erspImage(ERSP3-ERSP4, 'difference (NT1-NT2)',times, freq, obj.channels, sec);
            add(obj.chap, sec);
            
            hmfile = EEG.etc.src.hmfile;
            latency = ReportGenerator.findPeaksLatency(ERP(:,:,1), EEGArray(1).times);
            sec = ReportGenerator.erpSourceAnalysis(ERP(:,:,1), hmfile, 'difference (T1-T2)', latency, EEGArray(1).times);
            add(obj.chap, sec);
            
            latency = ReportGenerator.findPeaksLatency(ERP(:,:,2), EEGArray(1).times);
            sec = ReportGenerator.erpSourceAnalysis(ERP(:,:,2), hmfile, 'difference (NT1-NT2)', latency, EEGArray(1).times);
            add(obj.chap, sec);
            
            sec = ReportGenerator.trialStats(EEGArray, obj.channels);
            add(obj.chap, sec);
            chap = obj.chap;
        end
        
        function [eventType, latency, targets_correct, targets_incorrect, nontargets_correct, nontargets_incorrect] = extractEvents(obj, EEG)
            eventType = {EEG.event.type};
            eventLatency = cell2mat({EEG.event.latency});
            
            % Remove 0 and 99 events
            indz = ismember(eventType,{'0','99'});
            eventType(indz) = [];
            eventLatency(indz) = [];
            for k=1:length(eventType)
                eventType{k} = str2double(eventType{k});
            end
            eventType = cell2mat(eventType);
            indnan = isnan(eventType);
            eventType(indnan) = [];
            eventLatency(indnan) = [];
            
            targets_correct = strfind(eventType, [10,1,11,14]);
            targets_incorrect = strfind(eventType, [10,1,15]);
            nontargets_correct = strfind(eventType, [10,2,14]);
            nontargets_incorrect = strfind(eventType, [10,2,11,15]);
            
            startEvent = find(ismember(eventType,[1001,1002]));
            if isempty(startEvent)
                startEvent = find(ismember(eventType,[100]));
                startEvent(1) = [];
            end
            endEvent   = find(ismember(eventType,101));
            
            target = sort([targets_correct targets_incorrect]);
            nontarget = sort([nontargets_correct nontargets_incorrect]);
            
            target_1    = target(target > startEvent(1) & target < endEvent(2));
            nontarget_1 = nontarget(nontarget > startEvent(1) & nontarget < endEvent(2));
            
            target_2    = target(target > startEvent(end) & target < endEvent(end));
            nontarget_2 = nontarget(nontarget > startEvent(end) & nontarget < endEvent(end));
            
            latency = {eventLatency(target_1+1) eventLatency(nontarget_1+1) eventLatency(target_2+1) eventLatency(nontarget_2+1)};
            eventType = {'go_green_target_1','go_green_nontarget_1','go_green_target_2','go_green_nontarget_2'};
        end
    end
    methods(Static)
        %% Summary
        function sec = summary(targets_correct, targets_incorrect, nontargets_correct, nontargets_incorrect)
            sec = mlreportgen.report.Section;
            sec.Title = 'Summary correct and incorrect trials';
            d = [...
                length(targets_correct) length(targets_incorrect);...
                length(nontargets_correct), length(nontargets_incorrect)];
            D = cell(2);
            for i=1:2, for j=1:2, D{i,j} = sprintf('%0.2f',d(i,j));end;end
            t = table(D(:,1),D(:,2));
            t.Properties.RowNames = {'Targets','Nontargets'};
            t.Properties.VariableNames = {'Correct','Incorrect'};
            
            t = mlreportgen.dom.Table(t);
            t.Style = {...
                mlreportgen.dom.RowSep('solid','black','1px'),...
                mlreportgen.dom.ColSep('solid','black','1px'),};
            t.Border = 'double';
            t.TableEntriesStyle = {mlreportgen.dom.HAlign('center')};
            add(sec, mlreportgen.dom.Paragraph('Responses: '));
            add(sec,t);
            
            dp = 100*bsxfun(@rdivide, d,sum(d,2));
            D = cell(2);
            for i=1:2, for j=1:2, D{i,j} = sprintf('%0.2f',dp(i,j));end;end
            tp = table(D(:,1),D(:,2));
            tp.Properties.RowNames = {'Targets','Nontargets'};
            tp.Properties.VariableNames = {'Correct','Incorrect'};
            
            tp = mlreportgen.dom.Table(tp);
            tp.Style = {...
                mlreportgen.dom.RowSep('solid','black','1px'),...
                mlreportgen.dom.ColSep('solid','black','1px'),};
            tp.Border = 'double';
            tp.TableEntriesStyle = {mlreportgen.dom.HAlign('center')};
            
            add(sec, mlreportgen.dom.Paragraph('Responses (%): '));
            add(sec,tp);
        end
       
        %% Trial stats
        function sec = trialStats(EEG_target, EEG_nontarget, targetCh)
            sec = mlreportgen.report.Section;
            sec.Title = 'Trial stats';
            
            nch = length(targetCh);
            prcTrials = [0.1 0.15 0.25 0.5 0.6 0.7 0.8 0.9 1];
            trialNumber = round(EEG_target.trials*prcTrials);
            nt = length(trialNumber);
            x = zeros(nt, nch,2);
            erp = mean(EEG_target.data,3);
            for ch=1:nch
                indCh = find(ismember({EEG_target.chanlocs.labels},targetCh{ch}));
                for k=1:nt
                    x(k,ch,1) = corr(erp(indCh,:)',mean(EEG_target.data(indCh,:,1:trialNumber(k)),3)');
                end
            end
            
            trialNumber = round(EEG_nontarget.trials*prcTrials);
            erp = mean(EEG_nontarget.data,3);
            for ch=1:nch
                indCh = find(ismember({EEG_target.chanlocs.labels},targetCh{ch}));
                for k=1:nt
                    x(k,ch,2) = corr(erp(indCh,:)',mean(EEG_nontarget.data(indCh,:,1:trialNumber(k)),3)');
                end
            end
                        
            fig = figure('Position',[680   632   937   330], 'Tag','BrainEAnalyzerFig');
            subplot(121);
            plot(prcTrials*100,x(:,:,1),'-.')
            hold on;
            h = plot(prcTrials*100,x(:,:,1),'o');
            set(h(1),'color',[0 0.447 0.741]);
            set(h(2),'color',[0.85, 0.325, 0.098]);
            xlabel('Fraction of trials (%)')
            ylabel('Correlation');
            title('Target')
            grid on
            legend(targetCh,'Location','southeast');
            ylim([0.5 1]);
            xlim(prcTrials([1 end])*100)
            
            subplot(122);
            plot(prcTrials*100,x(:,:,2),'-.')
            hold on;
            h = plot(prcTrials*100,x(:,:,2),'o');
            set(h(1),'color',[0 0.447 0.741]);
            set(h(2),'color',[0.85, 0.325, 0.098]);
            xlabel('Fraction of trials (%)')
            title('Nontarget')
            grid on
            ylim([0.5 1])
            xlim(prcTrials([1 end])*100);
            
            fig = mlreportgen.report.Figure(fig);
            fig.Snapshot.Height = '2in';
            fig.Snapshot.Width = '5.5in';
            fig.Snapshot.Caption = sprintf('Correlation between the ERP and ERPs computed using a percentage of the total number of trials for target and nontarget conditions.');
            add(sec,fig);
        end
    end
end