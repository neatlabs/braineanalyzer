classdef LostStar < brainePkg.BrainEAssessment
    
    methods
        function obj = LostStar(latencies, channels, reportDir, doSourceAnalysis)
            obj = obj@brainePkg.BrainEAssessment('Lost Start', latencies, channels, reportDir, doSourceAnalysis);
        end
        
        %% Generate report
        function [chap, EEG] = generateReport(obj, EEG)
            condition = {'memory'};
            latencies = -100:100:4900;
            timelim = [-1 5];
            [eventType, latency, match_correct, match_incorrect, nonmatch_correct, nonmatch_incorrect] = obj.extractEvents(EEG);
            EEG = eeg_addnewevents(EEG,latency, eventType);
            
            sec = brainePkg.LostStar.summary(match_correct, match_incorrect, nonmatch_correct, nonmatch_incorrect);
            [sec, EEGArray] = ReportGenerator.epoching(EEG, eventType, condition, timelim, sec);
            add(obj.chap, sec);
            
            ERP = mean(EEGArray(1).data,3);
            sec = brainePkg.LostStar.topoplot(ERP, condition, latencies, EEG.chanlocs, EEGArray(1).times);
            for ch=1:length(obj.channels)
                sec = ReportGenerator.singleTrialAnalysis(EEGArray, obj.channels{ch}, sec);
            end
            baseline = EEGArray(1).times>-500 & EEGArray(1).times<=0;
            [ERSP, times, freq] = ReportGenerator.ersp(EEGArray(1), obj.channels, baseline);
            sec = ReportGenerator.erspImage(ERSP, 'Memory',times, freq, obj.channels, sec);
            add(obj.chap, sec);
            
            ERPAlpha = brainePkg.LostStar.filterAlpha(ERP, EEG.srate);
            ERPTheta = brainePkg.LostStar.filterTheta(ERP, EEG.srate);           
            hmfile = EEG.etc.src.hmfile;
            baseline = find(EEGArray(1).times >-100 & EEGArray(1).times <= 0);
            
            postStm = find(EEGArray(1).times >=100 & EEGArray(1).times <= 500);
            latency = ReportGenerator.findPeaksLatency(ERPAlpha, EEGArray(1).times, baseline, postStm);
            sec = ReportGenerator.erpSourceAnalysis(ERPAlpha, hmfile, 'Post-threshold stim. alpha', latency, EEGArray(1).times);
            add(obj.chap, sec);
            
            postStm = find(EEGArray(1).times >=1000 & EEGArray(1).times <= 4000);
            latency = ReportGenerator.findPeaksLatency(ERPTheta, EEGArray(1).times, baseline, postStm);
            sec = ReportGenerator.erpSourceAnalysis(ERPTheta, hmfile, 'Delay period theta', latency, EEGArray(1).times);
            add(obj.chap, sec);
            
            sec = ReportGenerator.trialStats(EEGArray, obj.channels);
            add(obj.chap, sec);
            chap = obj.chap;
        end
        
        function [eventType, latency, match_correct, match_incorrect, nonmatch_correct, nonmatch_incorrect] = extractEvents(obj,EEG)
            eventType = {EEG.event.type};
            eventLatency = cell2mat({EEG.event.latency});
            
            % Remove 0 and 99 events
            indz = ismember(eventType,{'0','99'});
            eventType(indz) = [];
            eventLatency(indz) = [];
            for k=1:length(eventType)
                eventType{k} = str2double(eventType{k});
            end
            eventType = cell2mat(eventType);
            indnan = isnan(eventType);
            eventType(indnan) = [];
            eventLatency(indnan) = [];
            
            [match_correct, match_incorrect, nonmatch_correct, nonmatch_incorrect] = deal([]);
            for k=1:8
                match_correct      = [match_correct      strfind(eventType, [10,k,21,12,14])];
                match_incorrect    = [match_incorrect    strfind(eventType, [10,k,21,13,15])];
                nonmatch_correct   = [nonmatch_correct   strfind(eventType, [10,k,22,13,14])];
                nonmatch_incorrect = [nonmatch_incorrect strfind(eventType, [10,k,22,12,15])];
            end
            startEvent = find(ismember(eventType,300));
            if isempty(startEvent)
                startEvent = find(ismember(eventType,3001));
            else
                startEvent = startEvent(2);
            end
            endEvent   = find(ismember(eventType,301));
            endEvent   = endEvent(end);
            
            match_correct(match_correct < startEvent | match_correct > endEvent) = [];
            match_incorrect(match_incorrect < startEvent | match_incorrect > endEvent) = [];
            nonmatch_correct(nonmatch_correct < startEvent | nonmatch_correct > endEvent) = [];
            nonmatch_incorrect(nonmatch_incorrect < startEvent | nonmatch_incorrect > endEvent) = [];
            
            memory_trials = sort([match_correct match_incorrect nonmatch_correct nonmatch_incorrect]);
            latency = {eventLatency(memory_trials+1)};
            eventType = {'memory'};
        end
    end
    methods(Static)
        %% Summary
        function sec = summary(match_correct, match_incorrect, nonmatch_correct, nonmatch_incorrect)
            sec = mlreportgen.report.Section;
            sec.Title = 'Summary of memory trials ';
            d = [...
                length(match_correct) length(match_incorrect);...
                length(nonmatch_correct), length(nonmatch_incorrect)];
            D = cell(2);
            for i=1:2, for j=1:2, D{i,j} = sprintf('%0.2f',d(i,j));end;end
            t = table(D(:,1),D(:,2));
            t.Properties.RowNames = {'Match','Nonmatch'};
            t.Properties.VariableNames = {'Correct','Incorrect'};
            
            t = mlreportgen.dom.Table(t);
            t.Style = {...
                mlreportgen.dom.RowSep('solid','black','1px'),...
                mlreportgen.dom.ColSep('solid','black','1px'),};
            t.Border = 'double';
            t.TableEntriesStyle = {mlreportgen.dom.HAlign('center')};
            add(sec, mlreportgen.dom.Paragraph('Responses: '));
            add(sec,t);
            
            dp = 100*bsxfun(@rdivide, d,sum(d,2));
            D = cell(2);
            for i=1:2, for j=1:2, D{i,j} = sprintf('%0.2f',dp(i,j));end;end
            tp = table(D(:,1),D(:,2));
            tp.Properties.RowNames = {'Match','Nonmatch'};
            tp.Properties.VariableNames = {'Correct','Incorrect'};
            
            tp = mlreportgen.dom.Table(tp);
            tp.Style = {...
                mlreportgen.dom.RowSep('solid','black','1px'),...
                mlreportgen.dom.ColSep('solid','black','1px'),};
            tp.Border = 'double';
            tp.TableEntriesStyle = {mlreportgen.dom.HAlign('center')};
            
            add(sec, mlreportgen.dom.Paragraph('Responses (%): '));
            add(sec,tp);
        end
        
        function sec = topoplot(ERP, condition, latencies, chanlocs, times)
            % Topoplots
            latLength = length(latencies);
            N = floor(latLength/10);
            n = ceil(latLength/N);
            mx = prctile(abs(ERP(:)),97.5);
                        
            indLatency = interp1(times,1:length(times), latencies,'nearest');
            
            fig = figure('Position',[613,402,1040,370],'Color',[1 1 1],'Tag','BrainEAnalyzerFig');
            panelCount = 1;
            for k=1:N
                for i=1:n
                    ax = subplot(N,n,panelCount);
                    topoplot(ERP(:,indLatency(panelCount)),chanlocs);
                    axis(ax,'equal','tight','on');
                    set(ax,'CLim',[-mx mx],'YTickLabel',[],'XTickLabel',[],'XColor',[1 1 1],'YColor',[1 1 1]);
                    if i==1
                        cond = [upper(condition{1}(1)) lower(condition{1}(2:end))]; 
                        ylabel(ax,cond);
                    end
                    xlabel(ax,[num2str(latencies(panelCount)) ' ms']);
                    panelCount = panelCount+1;
                    if panelCount>latLength
                        break;
                    end
                end
            end
            fig.Color = [1 1 1];
            colorbar('Position',[0.9331    0.6224    0.0130    0.2312]);
            colormap(bipolar(256,0.85));
            
            sec = mlreportgen.report.Section;
            sec.Title = 'ERP analysis';
            fig = mlreportgen.report.Figure(fig);
            fig.Snapshot.Height = '3in';
            fig.Snapshot.Width = '8in';
            fig.Snapshot.Caption = sprintf('ERP topographic summary.');
            add(sec,fig);
        end
        
        function ERPfilt = filterAlpha(ERP, srate)
            order = 250;      % Order
            Fc1  = 7;      % First Cutoff Frequency
            Fc2  = 13.5;     % Second Cutoff Frequency
            flag = 'scale';  % Sampling Flag
            Beta = 0.5;      % Window Parameter
            % Create the window vector for the design algorithm.
            win = kaiser(order+1, Beta);
            
            % Calculate the coefficients using the FIR1 function.
            b  = fir1(order, [Fc1 Fc2]/(srate/2), 'bandpass', win, flag);
            ERPfilt = filtfilt(b,1,ERP')';
        end
        function ERPfilt = filterTheta(ERP, srate)
            order = 250;      % Order
            Fc1  = 3.5;      % First Cutoff Frequency
            Fc2  = 7;     % Second Cutoff Frequency
            flag = 'scale';  % Sampling Flag
            Beta = 0.5;      % Window Parameter
            % Create the window vector for the design algorithm.
            win = kaiser(order+1, Beta);
            
            % Calculate the coefficients using the FIR1 function.
            b  = fir1(order, [Fc1 Fc2]/(srate/2), 'bandpass', win, flag);
            ERPfilt = filtfilt(b,1,ERP')';
        end
        
        %% Target vs Nontarget
        function [sec, EEG_match] = summaryERP(EEG, latencies)
            sec = mlreportgen.report.Section;
            sec.Title = 'ERP analysis (match vs nonmatch)';
            
            EEG_match = pop_epoch( EEG, {'lostStart_match', 'lostStart_nonmatch'}, [-0.5  2], 'epochinfo', 'yes');
            nt = EEG_match.trials;
            [EEG_match, rejTarget] = pop_autorej(EEG_match, 'nogui','on','eegplot','off');
            EEG_match = pop_rmbase( EEG_match, [500 0], [1 126]);
            EEG_match = eeg_checkset( EEG_match );
            
            d = [nt,length(rejTarget),100*length(rejTarget)./nt];
            D = cell(1,3);
            for j=1:3, D{1,j} = sprintf('%0.2f',d(1,j));end
            t = table(D(:,1),D(:,2),D(:,3));
            t.Properties.RowNames = {'Trials'};
            t.Properties.VariableNames = {'Total','Rejected','Percentage'};
            
            t = mlreportgen.dom.Table(t);
            t.Style = {...
                mlreportgen.dom.RowSep('solid','black','1px'),...
                mlreportgen.dom.ColSep('solid','black','1px'),};
            t.Border = 'double';
            t.TableEntriesStyle = {mlreportgen.dom.HAlign('center')};
            add(sec, mlreportgen.dom.Paragraph('Artifacts rejection summary: '));
            add(sec,t);
            
            % Topoplots
            latencies = -100:100:EEG_match.xmax*1000-100;
            n = length(latencies);
            pop_topoplot(EEG_match,1,latencies,'',[1 n]);
            fig_target = gcf;
            tmp = mean(EEG_match.data,3);
            mx = prctile(abs(tmp(:)),97.5)*2;
            fig = figure('Position',[613,402,1040,286],'Color',[1 1 1], 'Tag','BrainEAnalyzerFig');
            t = latencies;
            h = flipud(fig_target.Children);
            for i=1:n
                ax = subplot(2,n/2,i);
                copyobj(h(i).Children,ax);
                axis equal tight
                set(ax,'CLim',[-mx mx],'YTickLabel',[],'XTickLabel',[],'XColor',[1 1 1],'YColor',[1 1 1]);
                xlabel(ax,[num2str(t(i)) ' ms']);
            end
            colorbar(ax,'Position',[0.9331    0.6224    0.0130    0.2312]);
            close(fig_target);
            colormap(bipolar(256,0.85))
            
            fig = mlreportgen.report.Figure(fig);
            fig.Snapshot.Height = '2in';
            fig.Snapshot.Width = '8in';
            fig.Snapshot.Caption = sprintf('ERP topographic summary for all trials.');
            add(sec,fig);
        end
        
        function sec = erpImages(sec, EEG_match, targetCh)
            indCh = find(ismember({EEG_match.chanlocs.labels},targetCh));
            fig = figure('Tag','BrainEAnalyzerFig');
            img = imgaussfilt(squeeze(EEG_match.data(indCh,:,:)),3);
            mx = prctile(abs(img(:)),97.5)*2;
            ax = subplot(211);
            imagesc(img')
            set(ax,'YDir','normal','CLim',[-mx mx],'XTickLabels',[]);
            ylabel('Trials')
            title([EEG_match.chanlocs(indCh).labels])
            colormap(bipolar(256,0.85))
            
            ax = subplot(212);
            plot(EEG_match.times,mean(EEG_match.data(indCh,:,:),3))
            ax.XTick = EEG_match.times(1):250:EEG_match.times(end)-100;
            ylim([-mx mx])
            ylabel(['ERP (' '\muV)'])
            xlabel('Time (ms)')
            grid on;
            
            fig = mlreportgen.report.Figure(fig);
            fig.Snapshot.Height = '4in';
            fig.Snapshot.Width = '6in';
            fig.Snapshot.Caption = sprintf(['Channel ' targetCh ' ERP image.']);
            add(sec,fig);
        end
        
        %% Trial stats
        function sec = trialStats(EEG_match, targetCh)
            sec = mlreportgen.report.Section;
            sec.Title = 'Trial stats';
            
            nch = length(targetCh);
            prcTrials = [0.1 0.15 0.25 0.5 0.6 0.7 0.8 0.9 1];
            trialNumber = round(EEG_match.trials*prcTrials);
            nt = length(trialNumber);
            x = zeros(nt, nch);
            erp = mean(EEG_match.data,3);
            for ch=1:nch
                indCh = find(ismember({EEG_match.chanlocs.labels},targetCh{ch}));
                for k=1:nt
                    x(k,ch) = corr(erp(indCh,:)',mean(EEG_match.data(indCh,:,1:trialNumber(k)),3)');
                end
            end
                        
            fig = figure('Position',[680   632   937   330], 'Tag','BrainEAnalyzerFig');
            plot(prcTrials*100,x,'-.')
            hold on;
            h = plot(prcTrials*100,x,'o');
            set(h(1),'color',[0 0.447 0.741]);
            set(h(2),'color',[0.85, 0.325, 0.098]);
            xlabel('Fraction of trials (%)')
            ylabel('Correlation');
            grid on
            legend(targetCh,'Location','southeast');
            ylim([0.5 1]);
            xlim(prcTrials([1 end])*100)
            
            fig = mlreportgen.report.Figure(fig);
            fig.Snapshot.Height = '2in';
            fig.Snapshot.Width = '5.5in';
            fig.Snapshot.Caption = sprintf('Correlation between the ERP and ERPs computed using a percentage of the total number of trials.');
            add(sec,fig);
        end
    end
end