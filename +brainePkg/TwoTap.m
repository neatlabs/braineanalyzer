classdef TwoTap < brainePkg.BrainEAssessment

    methods
        function obj = TwoTap(latencies, channels, reportDir, doSourceAnalysis)
            obj = obj@brainePkg.BrainEAssessment('Two Tap', latencies, channels, reportDir, doSourceAnalysis);
        end
        
        %% Generate report
        function [chap, EEG] = generateReport(obj, EEG)
            condition = {'response'};
            timelim = [-2 2];
            
            [eventType, latency, RT] = obj.extractEvents(EEG);
            EEG = eeg_addnewevents(EEG,latency, eventType,{'RT'}, {RT});
            sec = brainePkg.TwoTap.summary(latency, RT);
            [sec, EEG] = ReportGenerator.epoching(EEG, eventType, condition, timelim, sec);
            add(obj.chap, sec);
            
            ERP = mean(EEG.data,3);
            sec = ReportGenerator.topoplot(ERP, condition, obj.latencies, EEG.chanlocs, EEG.times);
            for ch=1:length(obj.channels)
                sec = ReportGenerator.singleTrialAnalysis(EEG, obj.channels{ch}, sec, {RT});
            end
            baseline = EEG.times>-2000 & EEG.times<=0;
            [ERSP, times, freq] = ReportGenerator.ersp(EEG, obj.channels, baseline);
            sec = ReportGenerator.erspImage(ERSP, condition{1},times, freq, obj.channels, sec);
            add(obj.chap, sec);
           
            sec = ReportGenerator.trialStats(EEG, obj.channels);
            add(obj.chap, sec);
            chap = obj.chap;
        end
        
        function [eventType, latency, RT] = extractEvents(obj, EEG)
            eventType = {EEG.event.type};
            eventLatency = cell2mat({EEG.event.latency});
            
            % Remove zero events
            indz = ismember(eventType,'0');
            eventType(indz) = [];
            eventLatency(indz) = [];
            for k=1:length(eventType)
                eventType{k} = str2double(eventType{k});
            end
            eventType = cell2mat(eventType);
            indnan = isnan(eventType);
            eventType(indnan) = [];
            eventLatency(indnan) = [];
            startEvent = find(ismember(eventType,500));
            endEvent   = find(ismember(eventType,501));
            response = strfind(eventType, 11);
            response    = response(response > startEvent(1) & response < endEvent(2));
            latency = {eventLatency(response)};
            eventType = {'two_tap_response'};
            RT = diff([eventLatency(startEvent(1)) latency{1}]/EEG.srate);
        end
    end
    methods(Static)
        %% Summary
        function sec = summary(response, RT)
            sec = mlreportgen.report.Section;
            sec.Title = 'Summary of responses';
            add(sec, mlreportgen.dom.Paragraph(['Responses: ' num2str(length(response{1})) '.']));
            fig = figure('Tag','BrainEAnalyzerFig');
            hist(RT);
            xlabel('Reaction time');
            ylabel('Counts');
            fig = mlreportgen.report.Figure(fig);
            fig.Snapshot.Caption = 'Reaction time.';
            add(sec,fig);
        end
    end
end