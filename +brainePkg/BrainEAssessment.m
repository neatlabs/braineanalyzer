classdef BrainEAssessment < handle
    properties
        assessmentType
        chap
        reportDir
        eventType
        latencies
        channels
        doSourceAnalysis
    end
    methods
        function obj = BrainEAssessment(assessmentType, latencies, channels, reportDir, doSourceAnalysis)
            obj.chap = mlreportgen.report.Chapter;
            ind = strfind(assessmentType,' ');
            if ~isempty(ind)
                ind = ind+1;
            end
            ind = [1 ind];
            assessmentType(ind) = upper(assessmentType(ind));
            obj.assessmentType = assessmentType;
            obj.chap.Title = assessmentType;
            obj.latencies = latencies;
            obj.channels = channels;
            obj.reportDir = reportDir;
            obj.doSourceAnalysis = doSourceAnalysis;
        end
    end
end