classdef Rest < brainePkg.BrainEAssessment

    methods
        function obj = Rest(latencies, channels, reportDir, doSourceAnalysis)
            obj = obj@brainePkg.BrainEAssessment('Rest', latencies, channels, reportDir, doSourceAnalysis);
        end
        
        %% Generate report
        function [chap, EEG] = generateReport(obj, EEG)
            [~, ~, EEG] = obj.extractEvents(EEG);
            
            fig = figure; 
            pop_spectopo(EEG, 1, EEG.times([1 end]), 'EEG' , 'freq', [6 11 22], 'freqrange',[2 25],'electrodes','off');
            fig.Children(6).YLabel.String = 'PSD (dB)';
            fig.Tag = 'BrainEAnalyzerFig';
            
            indCh = find(ismember({EEG.chanlocs.labels},obj.channels));
            h = flipud(fig.Children(6).Children(4:end));
            color = lines(length(indCh));
            set(h,'linewidth',0.5,'Color',[0.75 0.75 0.75]);
            for i=1:length(indCh)
                set(h(indCh(i)),'linewidth',2,'Color',color(i,:));
            end
            
            sec = mlreportgen.report.Section;
            sec.Title = 'Power spectral density.';
            fig = mlreportgen.report.Figure(fig);
            %fig.Snapshot.Height = '3in';
            %fig.Snapshot.Width = '8in';
            fig.Snapshot.Caption = 'Power spectral density (PSD). The blue and orange traces represent channels Fz anbd P4 respectively.';
            add(sec,fig);
            add(obj.chap, sec);
            chap = obj.chap;
        end
        
        function [eventType, latency, EEG] = extractEvents(obj, EEG)
            eventType = {EEG.event.type};            
            startEvent = find(ismember(eventType,'700'));
            endEvent   = find(ismember(eventType,'701'));
            latency = [];
            eventType = [];
            EEG = pop_select(EEG,'point',[EEG.event(startEvent).latency EEG.event(endEvent).latency+1]);
        end
    end
    methods(Static)
        %% Summary
        function sec = summary(response)
            sec = mlreportgen.report.Section;
            sec.Title = 'Summary of responses';
            add(sec, mlreportgen.dom.Paragraph(['Responses: ' num2str(length(response)) '.']));
        end
    end
end