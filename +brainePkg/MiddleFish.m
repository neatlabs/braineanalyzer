classdef MiddleFish < brainePkg.BrainEAssessment
    
    methods
        function obj = MiddleFish(latencies, channels, reportDir, doSourceAnalysis)
            obj = obj@brainePkg.BrainEAssessment('Middle Fish', latencies, channels, reportDir, doSourceAnalysis);
        end
        
        %% Generate report
        function [chap, EEG] = generateReport(obj, EEG)
            condition = {'congruent', 'incongruent'};
            timelim = [-1 1];
            
            [eventType, latency, targets_correct, targets_incorrect, nontargets_correct, nontargets_incorrect] = obj.extractEvents(EEG);
            EEG = eeg_addnewevents(EEG,latency, eventType);
            sec = brainePkg.MiddleFish.summary(targets_correct, targets_incorrect, nontargets_correct, nontargets_incorrect);
            [sec, EEGArray] = ReportGenerator.epoching(EEG, eventType, condition, timelim, sec);
            add(obj.chap, sec);
            
            ERP = zeros(EEG.nbchan, EEGArray(1).pnts,3);
            ERP(:,:,1) = mean(EEGArray(1).data,3);
            ERP(:,:,2) = mean(EEGArray(2).data,3);
            ERP(:,:,3) = ERP(:,:,2)-ERP(:,:,1);
            condition = {'congruent', 'incongruent', 'difference (I-C)'};
            sec = ReportGenerator.topoplot(ERP, condition, obj.latencies, EEG.chanlocs, EEGArray(1).times);
            
            for ch=1:length(obj.channels)
                sec = ReportGenerator.singleTrialAnalysis(EEGArray, obj.channels{ch}, sec);
            end
            baseline = EEGArray(1).times>-500 & EEGArray(1).times<=0;
            [ERSP_c, times, freq] = ReportGenerator.ersp(EEGArray(1), obj.channels, baseline);% congruent
            [ERSP_i, times, freq] = ReportGenerator.ersp(EEGArray(2), obj.channels, baseline);% Incongruent
            sec = ReportGenerator.erspImage(ERSP_i-ERSP_c, 'difference (I-C)',times, freq, obj.channels, sec);
            add(obj.chap, sec);
            
            hmfile = EEG.etc.src.hmfile;
            latency = ReportGenerator.findPeaksLatency(ERP(:,:,3), EEGArray(1).times);
            sec = ReportGenerator.erpSourceAnalysis(ERP(:,:,3), hmfile, 'difference (I-C)', latency, EEGArray(1).times);
            add(obj.chap, sec);
            
            sec = ReportGenerator.trialStats(EEGArray(1:2), obj.channels);
            add(obj.chap, sec);
            chap = obj.chap;
        end
        
        function [eventType, latency, targets_correct, targets_incorrect, nontargets_correct, nontargets_incorrect] = extractEvents(obj, EEG)
            
            eventType = {EEG.event.type};
            eventLatency = cell2mat({EEG.event.latency});
            
            % Remove 0 and 99 events
            indz = ismember(eventType,{'0','99'});
            eventType(indz) = [];
            eventLatency(indz) = [];
            for k=1:length(eventType)
                eventType{k} = str2double(eventType{k});
            end
            eventType = cell2mat(eventType);
            indnan = isnan(eventType);
            eventType(indnan) = [];
            eventLatency(indnan) = [];
            
            targets_correct      = [strfind(eventType, [10,1,12,14]) strfind(eventType, [10,2,13,14])];
            targets_incorrect    = [strfind(eventType, [10,1,13,15]) strfind(eventType, [10,2,12,15])];
            nontargets_correct   = [strfind(eventType, [10,3,12,14]) strfind(eventType, [10,4,13,14])];
            nontargets_incorrect = [strfind(eventType, [10,3,13,15]) strfind(eventType, [10,4,12,15])];
            
            startEvent = find(ismember(eventType,[2001, 2002]));
            if isempty(startEvent)
                startEvent = find(ismember(eventType,[200]));
                startEvent = startEvent(2);
            end
            endEvent   = find(ismember(eventType,201));
            startEvent = startEvent(1);
            endEvent   = endEvent(end);
            
            targets_correct(targets_correct < startEvent | targets_correct > endEvent) = [];
            targets_incorrect(targets_incorrect < startEvent | targets_incorrect > endEvent) = [];
            nontargets_correct(nontargets_correct < startEvent | nontargets_correct > endEvent) = [];
            nontargets_incorrect(nontargets_incorrect < startEvent | nontargets_incorrect > endEvent) = [];
            
            congruent = sort([targets_correct targets_incorrect]);
            incongruent = sort([nontargets_correct nontargets_incorrect]);
            
            latency = {eventLatency(congruent+1) eventLatency(incongruent+1)};
            eventType = {'middle_fish_congruent','middle_fish_incongruent'};
        end
    end
        
    methods(Static)
        
        %% Summary
        function sec = summary(targets_correct, targets_incorrect, nontargets_correct, nontargets_incorrect)
            sec = mlreportgen.report.Section;
            sec.Title = 'Summary correct and incorrect trials';
            d = [...
                length(targets_correct) length(targets_incorrect);...
                length(nontargets_correct), length(nontargets_incorrect)];
            D = cell(2);
            for i=1:2, for j=1:2, D{i,j} = sprintf('%0.2f',d(i,j));end;end
            t = table(D(:,1),D(:,2));
            t.Properties.RowNames = {'Congruent Targets','Incongruent Targets'};
            t.Properties.VariableNames = {'Correct','Incorrect'};
            
            t = mlreportgen.dom.Table(t);
            t.Style = {...
                mlreportgen.dom.RowSep('solid','black','1px'),...
                mlreportgen.dom.ColSep('solid','black','1px'),};
            t.Border = 'double';
            t.TableEntriesStyle = {mlreportgen.dom.HAlign('center')};
            add(sec, mlreportgen.dom.Paragraph('Responses: '));
            add(sec,t);
            
            dp = 100*bsxfun(@rdivide, d,sum(d,2));
            for i=1:2, for j=1:2, D{i,j} = sprintf('%0.2f',dp(i,j));end;end
            tp = table(D(:,1),D(:,2));
            tp.Properties.RowNames = {'Congruent Targets','Incongruent Targets'};
            tp.Properties.VariableNames = {'Correct','Incorrect'};
            
            tp = mlreportgen.dom.Table(tp);
            tp.Style = {...
                mlreportgen.dom.RowSep('solid','black','1px'),...
                mlreportgen.dom.ColSep('solid','black','1px'),};
            tp.Border = 'double';
            tp.TableEntriesStyle = {mlreportgen.dom.HAlign('center')};
            
            add(sec, mlreportgen.dom.Paragraph('Responses (%): '));
            add(sec,tp);
        end
        
%         %% Target vs Nontarget
%         function [sec, EEG_congruent, EEG_incongruent] = summaryERP(EEG, latencies)
%             sec = mlreportgen.report.Section;
%             sec.Title = 'ERP analysis (congruent vs incongruent)';
%             
%             EEG_congruent = pop_epoch( EEG, {'middle_fish_congruent'}, [-1  1], 'epochinfo', 'yes');
%             nt = EEG_congruent.trials;
%             [EEG_congruent, rejTarget] = pop_autorej(EEG_congruent, 'nogui','on','eegplot','off');
%             EEG_congruent = pop_rmbase( EEG_congruent, [500 0], [1 126]);
%             EEG_congruent = eeg_checkset( EEG_congruent );
%             
%             EEG_incongruent = pop_epoch( EEG, {'middle_fish_incongruent'}, [-1  1], 'epochinfo', 'yes');
%             nnt = EEG_incongruent.trials;
%             [EEG_incongruent, rejNontarget] = pop_autorej(EEG_incongruent, 'nogui','on','eegplot','off');
%             EEG_incongruent = pop_rmbase( EEG_incongruent, [500 0], [1 126]);
%             EEG_incongruent = eeg_checkset( EEG_incongruent );
%             
%             D = cell(2,3);
%             d = [[nt;nnt],[length(rejTarget); length(rejNontarget)],100*[length(rejTarget); length(rejNontarget)]./[nt;nnt]];
%             for i=1:2, for j=1:3, D{i,j} = sprintf('%0.2f',d(i,j));end;end
%             
%             t = table(D(:,1),D(:,2),D(:,3));
%             t.Properties.RowNames = {'Congruent Targets','Incongruent Targets'};
%             t.Properties.VariableNames = {'Total','Rejected','Percentage'};
%             
%             t = mlreportgen.dom.Table(t);
%             t.Style = {...
%                 mlreportgen.dom.RowSep('solid','black','1px'),...
%                 mlreportgen.dom.ColSep('solid','black','1px'),};
%             t.Border = 'double';
%             t.TableEntriesStyle = {mlreportgen.dom.HAlign('center')};
%             add(sec, mlreportgen.dom.Paragraph('Artifacts rejection summary: '));
%             add(sec,t);
%             
%             
%             % Topoplots
%             n = length(latencies);
%             pop_topoplot(EEG_congruent,1,latencies,'',[1 n]);
%             fig_target = gcf;
%             pop_topoplot(EEG_incongruent,1,latencies,'',[1 n]);
%             fig_nontarget = gcf;
%             tmp1 = mean(EEG_congruent.data,3);
%             tmp2 = mean(EEG_incongruent.data,3);
%             mx = max([prctile(abs(tmp1(:)),97.5)*2 prctile(abs(tmp2(:)),97.5)*2]);
%             fig = figure('Position',[613,402,1040,370],'Color',[1 1 1],'Tag','BrainEAnalyzerFig');
%             t = latencies;
%             h = flipud(fig_target.Children);
%             for i=1:n
%                 ax = subplot(3,n,i);
%                 copyobj(h(i).Children,ax);
%                 axis equal tight
%                 set(ax,'CLim',[-mx mx],'YTickLabel',[],'XTickLabel',[],'XColor',[1 1 1],'YColor',[1 1 1]);
%                 if i==1
%                     ylabel(ax,'Congruent');
%                 end
%             end
%             colorbar(ax,'Position',[0.9331    0.6224    0.0130    0.2312]);
%             close(fig_target);
%             
%             h = flipud(fig_nontarget.Children);
%             for i=1:n
%                 ax = subplot(3,n,i+n);
%                 copyobj(h(i).Children,ax);
%                 axis equal tight
%                 set(ax,'CLim',[-mx mx],'YTickLabel',[],'XTickLabel',[],'XColor',[1 1 1],'YColor',[1 1 1]);
%                 if i==1
%                     ylabel(ax,'Incongruent');
%                 end
%             end
%             colormap(ax,bipolar(256,0.85))
%             close(fig_nontarget);
%             
%             % Difference
%             erp_diff = mean(EEG_incongruent.data,3) - mean(EEG_congruent.data,3);
%             mx = prctile(abs(erp_diff(:)),95);
%             for i=1:n
%                 ax = subplot(3,n,i+2*n);
%                 cla(ax);
%                 topoplot(erp_diff(:,i),EEG_congruent.chanlocs);
%                 axis equal tight
%                 set(ax,'CLim',[-mx mx],'YTickLabel',[],'XTickLabel',[],'XColor',[1 1 1],'YColor',[1 1 1],'Visible','on');
%                 xlabel(ax,[num2str(t(i)) ' ms']);
%                 if i==1
%                     ylabel(ax,'Difference (I-C)');
%                 end
%             end
%             colormap(bipolar(256,0.85));
%             set(fig,'Color',[1 1 1])
%             
%             fig = mlreportgen.report.Figure(fig);
%             fig.Snapshot.Height = '2in';
%             fig.Snapshot.Width = '8in';
%             fig.Snapshot.Caption = sprintf('ERP topographic summary for congruent and incongruent conditions.');
%             add(sec,fig);
%         end
%         
%         function sec = erpImages(sec, EEG_congruent, EEG_incongruent, targetCh)
%             indCh = find(ismember({EEG_congruent.chanlocs.labels},targetCh));
%             fig = figure('Tag','BrainEAnalyzerFig');
%             img = imgaussfilt(squeeze(EEG_congruent.data(indCh,:,:)),3);
%             mx = prctile(abs(img(:)),97.5)*2;
%             ax = subplot(221);
%             imagesc(img')
%             set(ax,'YDir','normal','CLim',[-mx mx],'XTickLabels',[]);
%             ylabel('Trials')
%             title(['Congruent (' EEG_congruent.chanlocs(indCh).labels ')'])
%             colormap(bipolar(256,0.85))
%             
%             ax = subplot(223);
%             plot(EEG_congruent.times,mean(EEG_congruent.data(indCh,:,:),3))
%             ax.XTick = EEG_congruent.times:250:EEG_congruent.times(end);
%             ylim([-mx mx])
%             ylabel(['ERP (' '\muV)'])
%             xlabel('Time (ms)')
%             grid on;
%             
%             img = imgaussfilt(squeeze(EEG_incongruent.data(indCh,:,:)),3);
%             ax = subplot(222);
%             imagesc(img')
%             set(ax,'YDir','normal','CLim',[-mx mx],'XTickLabels',[]);
%             title(['Incongruent (' EEG_incongruent.chanlocs(indCh).labels ')'])
%             colorbar('Position',[0.9236    0.5841    0.0139    0.3405]);
%             
%             ax = subplot(224);
%             plot(EEG_incongruent.times,mean(EEG_incongruent.data(indCh,:,:),3))
%             ax.XTick = EEG_congruent.times(1):250:EEG_congruent.times(end);
%             ylim([-mx mx])
%             xlabel('Time (ms)')
%             grid on;
%             
%             fig = mlreportgen.report.Figure(fig);
%             fig.Snapshot.Height = '4in';
%             fig.Snapshot.Width = '6in';
%             fig.Snapshot.Caption = sprintf(['Channel ' targetCh 'ERP image for congruent and incongruent trials.']);
%             add(sec,fig);
%             
%         end
%         
%         function sec = erps(sec, EEG_congruent, EEG_incongruent, targetCh)
%             erp_diff = mean(EEG_incongruent.data,3) - mean(EEG_congruent.data,3);
%             %figure;
%             %newtimef( squeeze(erp_diff(ind,:)),EEG_incongruent.pnts,[-500 1000],EEG_incongruent.srate, 0,'plotitc','off','baseline',[-500 0]);
%             
%             n = length(targetCh);
%             cmap = bipolar(256,0.75);
%             fig = figure('Tag','BrainEAnalyzerFig');
%             fig.Position(3:4) = [971   367];
%             for ch=1:n
%                 ind = ismember({EEG_congruent.chanlocs.labels},targetCh{ch});
%                 [wt,f] = cwt(squeeze(erp_diff(ind,:)), EEG_incongruent.srate);
%                 ersp = abs(wt);
%                 ersp = bsxfun(@minus,ersp,mean(ersp(:,EEG_incongruent.times>-500 & EEG_incongruent.times<=0),2));
%                 ax = subplot(1,n,ch);
%                 imagesc(ax,EEG_incongruent.times,f,ersp);
%                 set(ax,'YDir','normal','YScale','log','YTick',[1:5 10:10:40]);
%                 hold(ax,'on');
%                 plot(ax,[0 0],ylim,'k-.');
%                 set(ax,'ylim',[1,40]);
%                 if ch==1
%                     ylabel('Frequency (Hz)')
%                 end
%                 grid(ax,'on');
%                 xlabel('Time (ms)')
%                 title(['ERSP (I-C ' targetCh{ch} ')'])
%                 colormap(cmap)
%                 colorbar(ax,'Position',[0.9324    0.1090    0.0275    0.8147])
%             end
%             fig = mlreportgen.report.Figure(fig);
%             fig.Snapshot.Height = '4in';
%             fig.Snapshot.Width = '6in';
%             fig.Snapshot.Caption = sprintf(['ERSP of the difference ERP incongruent minus congruent.']);
%             add(sec,fig);
%         end
%         
%         %% Trial stats
%         function sec = trialStats(EEG_congruent, EEG_incongruent, targetCh)
%             sec = mlreportgen.report.Section;
%             sec.Title = 'Trial stats';
%             
%             nch = length(targetCh);
%             prcTrials = [0.1 0.15 0.25 0.5 0.6 0.7 0.8 0.9 1];
%             trialNumber = round(EEG_congruent.trials*prcTrials);
%             nt = length(trialNumber);
%             x = zeros(nt, nch,2);
%             erp = mean(EEG_congruent.data,3);
%             for ch=1:nch
%                 indCh = find(ismember({EEG_congruent.chanlocs.labels},targetCh{ch}));
%                 for k=1:nt
%                     x(k,ch,1) = corr(erp(indCh,:)',mean(EEG_congruent.data(indCh,:,1:trialNumber(k)),3)');
%                 end
%             end
%             
%             trialNumber = round(EEG_incongruent.trials*prcTrials);
%             erp = mean(EEG_incongruent.data,3);
%             for ch=1:nch
%                 indCh = find(ismember({EEG_congruent.chanlocs.labels},targetCh{ch}));
%                 for k=1:nt
%                     x(k,ch,2) = corr(erp(indCh,:)',mean(EEG_incongruent.data(indCh,:,1:trialNumber(k)),3)');
%                 end
%             end
%                         
%             fig = figure('Position',[680   632   937   330],'Tag','BrainEAnalyzerFig');
%             subplot(121);
%             plot(prcTrials*100,x(:,:,1),'-.')
%             hold on;
%             h = plot(prcTrials*100,x(:,:,1),'o');
%             set(h(1),'color',[0 0.447 0.741]);
%             set(h(2),'color',[0.85, 0.325, 0.098]);
%             xlabel('Fraction of trials (%)')
%             ylabel('Correlation');
%             title('Congruent')
%             grid on
%             legend(targetCh,'Location','southeast');
%             ylim([0.5 1]);
%             xlim(prcTrials([1 end])*100)
%             
%             subplot(122);
%             plot(prcTrials*100,x(:,:,2),'-.')
%             hold on;
%             h = plot(prcTrials*100,x(:,:,2),'o');
%             set(h(1),'color',[0 0.447 0.741]);
%             set(h(2),'color',[0.85, 0.325, 0.098]);
%             xlabel('Fraction of trials (%)')
%             title('Incongruent')
%             grid on
%             ylim([0.5 1])
%             xlim(prcTrials([1 end])*100);
%             
%             fig = mlreportgen.report.Figure(fig);
%             fig.Snapshot.Height = '2in';
%             fig.Snapshot.Width = '5.5in';
%             fig.Snapshot.Caption = sprintf('Correlation between the ERP and ERPs computed using a percentage of the total number of trials for congruent and incongruent conditions.');
%             add(sec,fig);
%         end
%         
%         function sec = estimateSources(EEG_congruent, EEG_incongruent)
%             erp_diff = mean(EEG_incongruent.data,3) - mean(EEG_congruent.data,3);
%             baseline = find(EEG_congruent.times >-100 & EEG_congruent.times <= 0);
%             postStm = find(EEG_congruent.times >=100 & EEG_congruent.times <= 500);
%             mx = [];
%             mn = [];
%             for ch=1:EEG_congruent.nbchan
%                 mx = [mx findpeaks(erp_diff(ch,baseline))];
%                 [~,loc] = findpeaks(-erp_diff(ch,baseline));
%                 mn = [mn erp_diff(ch,baseline(loc))];
%             end
%             th_mn = prctile(mn,5);
%             th_mx = prctile(mx,95);
%             
%             loc_mx = [];
%             loc_mn = [];
%             for ch=1:EEG_congruent.nbchan
%                 [pk,loc] = findpeaks(erp_diff(ch,postStm));
%                 loc_mx = [loc_mx loc(pk > th_mx)];
%                 [~,loc] = findpeaks(-erp_diff(ch,postStm));
%                 pk = erp_diff(ch,postStm(loc));
%                 loc_mn = [loc_mn loc(pk < th_mn)];
%             end
%             loc_mx = unique(loc_mx);
%             loc_mx(diff(loc_mx) < 3) = [];
%             loc_mn = unique(loc_mn);
%             loc_mn(diff(loc_mn) < 3) = [];
%             latency = [loc_mx loc_mn];
%             n = length(latency);
%             hm = headModel.loadFromFile(EEG_congruent.etc.src.hmfile);
%             X = zeros(size(hm.K,2),n);
%             solver = bsbl(hm);
%             
%             for k=1:n
%                 ind = postStm(latency(k))+(-5:5);
%                 x = solver.update(erp_diff(:,ind));
%                 X(:,k) = mean(abs(x),2);
%             end
%             
%             cmap = bipolar(256,0.75);
%             clim = [0 prctile(X(:),95)];
%             fig = figure('Tag','BrainEAnalyzerFig');
%             fig.Position(3:4) = [1363 661];
%             for k=1:n
%                 ax = subplot(3,n,k);
%                 patch('vertices',hm.cortex.vertices,'Faces',hm.cortex.faces,'FaceVertexCData',X(:,k),...
%                     'FaceColor','interp','FaceLighting','phong','LineStyle','none','FaceAlpha',0.3,'SpecularColorReflectance',0,...
%                     'SpecularExponent',25,'SpecularStrength',0.25,'Parent',ax);
%                 set(ax,'Clim',clim,'XTick',[],'Ytick',[],'Ztick',[],'XColor',[1 1 1],'YColor',[1 1 1], 'ZColor',[1 1 1]);
%                 axis(ax, 'vis3d','equal','tight')
%                 view([-90 90])
%                 if k==1
%                     xlabel(ax,'Dorsal');
%                 end
%                 
%                 ax = subplot(3,n,k+n);
%                 patch('vertices',hm.cortex.vertices,'Faces',hm.cortex.faces,'FaceVertexCData',X(:,k),...
%                     'FaceColor','interp','FaceLighting','phong','LineStyle','none','FaceAlpha',0.3,'SpecularColorReflectance',0,...
%                     'SpecularExponent',25,'SpecularStrength',0.25,'Parent',ax);
%                 set(ax,'Clim',clim,'XTick',[],'Ytick',[],'Ztick',[],'XColor',[1 1 1],'YColor',[1 1 1], 'ZColor',[1 1 1]);
%                 axis(ax, 'vis3d','equal','tight')
%                 view([0 0])
%                 if k==1
%                     zlabel(ax,'Lateral');
%                 end
%                 
%                 ax = subplot(3,n,k+2*n);
%                 patch('vertices',hm.cortex.vertices,'Faces',hm.cortex.faces,'FaceVertexCData',X(:,k),...
%                     'FaceColor','interp','FaceLighting','phong','LineStyle','none','FaceAlpha',0.3,'SpecularColorReflectance',0,...
%                     'SpecularExponent',25,'SpecularStrength',0.25,'Parent',ax);
%                 set(ax,'Clim',clim,'XTick',[],'Ytick',[],'Ztick',[],'XColor',[1 1 1],'YColor',[1 1 1], 'ZColor',[1 1 1]);
%                 axis(ax, 'vis3d','equal','tight')
%                 view(ax,[90 0]);
%                 if k==1
%                     zlabel(ax,'Frontal');
%                 end
%                 ylabel(ax,[num2str(EEG_incongruent.times(postStm(latency(k)))) ' ms'])
%             end
%             colormap(cmap(end/2:end,:));
%             
%             sec = mlreportgen.report.Section;
%             sec.Title = 'ERP source analysis';
%             fig = mlreportgen.report.Figure(fig);
%             fig.Snapshot.Height = '2in';
%             fig.Snapshot.Width = '8in';
%             fig.Snapshot.Caption = sprintf('ERP source summary for incongruent-congruent.');
%             add(sec,fig);
%         end
    end
end