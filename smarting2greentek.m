function EEG = smarting2greentek(EEG)
ind = find(ismember({EEG.chanlocs.labels},{'FC3','FC4','FCz','Oz'}));
if length(ind)==4
    return
end
ind = find(ismember({EEG.chanlocs.labels},{'T7','T8','M1','M2'}));

xyz = [cell2mat({EEG.chanlocs.X})' cell2mat({EEG.chanlocs.Y})' cell2mat({EEG.chanlocs.Z})'];
if isempty(xyz)
    return;
end

% FC3 = (F3+C3)/2
loc = ismember({EEG.chanlocs.labels},{'F3','C3'});
xyz(ind(1),:) = mean(xyz(loc,:));

% FC4 = (F4+C4)/2
loc = ismember({EEG.chanlocs.labels},{'F4','C4'});
xyz(ind(2),:) = mean(xyz(loc,:));

% FCz = (Fz+Cz)/2
loc = ismember({EEG.chanlocs.labels},{'Fz','Cz'});
xyz(ind(3),:) = mean(xyz(loc,:));

% Oz = (O1+O2)/2
loc = ismember({EEG.chanlocs.labels},{'O1','O2'});
xyz(ind(4),:) = mean(xyz(loc,:));


EEG.chanlocs(ind(1)).labels = 'FC3';
EEG.chanlocs(ind(1)).X = xyz(ind(1),1);
EEG.chanlocs(ind(1)).Y = xyz(ind(1),2);
EEG.chanlocs(ind(1)).Z = xyz(ind(1),3);

EEG.chanlocs(ind(2)).labels = 'FC4';
EEG.chanlocs(ind(2)).X = xyz(ind(2),1);
EEG.chanlocs(ind(2)).Y = xyz(ind(2),2);
EEG.chanlocs(ind(2)).Z = xyz(ind(2),3);

EEG.chanlocs(ind(3)).labels = 'FCz';
EEG.chanlocs(ind(3)).X = xyz(ind(3),1);
EEG.chanlocs(ind(3)).Y = xyz(ind(3),2);
EEG.chanlocs(ind(3)).Z = xyz(ind(3),3);

EEG.chanlocs(ind(4)).labels = 'Oz';
EEG.chanlocs(ind(4)).X = xyz(ind(4),1);
EEG.chanlocs(ind(4)).Y = xyz(ind(4),2);
EEG.chanlocs(ind(4)).Z = xyz(ind(4),3);

end