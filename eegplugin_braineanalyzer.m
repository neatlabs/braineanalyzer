function vers = eegplugin_braineanalyzer(fig,try_strings, catch_strings)
vers = 'BrainE Analyzer';
libPath = fileparts(which('eeglab'));
addpath(fullfile(libPath, 'plugins', 'braineanalyzer'));
