classdef BrainEAnalyzer < handle
    properties
        dataFile
        reportFile
        reportType
        gui
    end
    properties(GetAccess=private)
        reportGenerator
    end
    methods
        function obj = BrainEAnalyzer(dataFile, reportFile, reportType)
            if nargin < 1
                dataFile = [];
            end
            if nargin < 2
                reportFile = [];
            end
            if nargin < 3
                reportType = 'pdf';
            end
            obj.dataFile = dataFile;
            obj.reportFile = reportFile;
            obj.reportType = reportType;
            obj.show;
        end
        function run(obj, dataFile, reportFile, latencies, channels, cutoffFreq, doSourceAnalysis)
            obj.dataFile = dataFile;
            obj.reportFile = reportFile;
            set(obj.gui.Window,'pointer','watch');
            drawnow;
            obj.reportGenerator = BrainEReport('BrainE Assessments', obj.reportType, obj.reportFile);
            obj.reportFile = obj.reportGenerator.generateReport(obj.dataFile, latencies, channels, cutoffFreq, doSourceAnalysis);
            set(obj.gui.Window,'pointer','arrow');
        end
        
        function gui = show(obj)
            fig = findall(0,'Tag','BrainEAnalyzer');
            if ~isempty(fig)
                obj.gui = fig;
                gui = fig;
                return;
            end
            obj.gui = struct();
            obj.gui.Window = figure( ...
                'Name', 'BrainE Analyzer', ...
                'NumberTitle', 'off', ...
                'MenuBar', 'none', ...
                'Toolbar', 'none', ...
                'HandleVisibility', 'off',...
                'Tag','BrainEAnalyzer',...
                'Visible','off');
            
            try
                verticalLayout = uix.VBoxFlex( 'Parent', obj.gui.Window, 'Spacing', 2 );
                topPanel = uix.Panel( 'Parent', verticalLayout, 'Position',[1  448.2536  732.0000   30.7464]);
                
                topPanelVLayout = uix.VBoxFlex( 'Parent', topPanel, 'Spacing', 2 );
                
                topPanelHLayout1 = uix.HBoxFlex( 'Parent', topPanelVLayout, 'Spacing', 3);
                uicontrol( 'Parent', topPanelHLayout1, 'style','text', 'String', 'Data file','Units','Normalized');
                obj.gui.dataFile = uicontrol( 'Parent', topPanelHLayout1, 'style', 'edit', 'Enable','on','Units','Normalized', 'Callback',@BrainEAnalyzer.onSetDataFile,'UserData',obj);
                uicontrol( 'Parent', topPanelHLayout1, 'style', 'pushbutton', 'String','Select', 'Callback',@BrainEAnalyzer.onSelect,'Units','Normalized','UserData',obj);
                
                topPanelHLayout2 = uix.HBoxFlex( 'Parent', topPanelVLayout, 'Spacing', 3);
                uicontrol( 'Parent', topPanelHLayout2, 'style','text', 'String', 'Report file','Units','Normalized');
                obj.gui.reportFile = uicontrol( 'Parent', topPanelHLayout2, 'style', 'edit', 'Enable','on','Units','Normalized');
                btn2 = uicontrol( 'Parent', topPanelHLayout2, 'style', 'pushbutton', 'String','Save', 'Callback',@BrainEAnalyzer.onSave,'Units','Normalized','UserData',obj);
                
                topPanelHLayout1.Widths = -[1; 4; 2];
                topPanelHLayout2.Widths = -[1; 4; 2];
                topPanelVLayout.Heights = -[1 1];
                
                bottomPanel = uix.Panel( 'Parent', verticalLayout);
                verticalLayout.Heights = -[1 6];
                bottomPanelVLayout = uix.VBoxFlex( 'Parent', bottomPanel, 'Spacing', 2 );
                
                bottomPanel1 = uix.Panel( 'Parent', bottomPanelVLayout);
                bottomBox = uix.HButtonBox( 'Parent', bottomPanelVLayout);
                uicontrol( 'Parent', bottomBox, 'style', 'pushbutton', 'String','Run', 'Callback',@BrainEAnalyzer.onRun,'Units','Normalized','UserData',obj);
                uicontrol( 'Parent', bottomBox, 'style', 'pushbutton', 'String','View', 'Callback',@BrainEAnalyzer.onView,'Units','Normalized','UserData',obj);
                uicontrol( 'Parent', bottomBox, 'style', 'pushbutton', 'String','Close all', 'Callback',@BrainEAnalyzer.onCloseAll,'Units','Normalized','UserData',obj);
                uicontrol( 'Parent', bottomBox, 'style', 'pushbutton', 'String','Cancel', 'Callback',@BrainEAnalyzer.onCancel,'Units','Normalized','UserData',obj);
                bottomPanelVLayout.Heights = -[10 1];
                
                bottomPanel1HLayout = uix.HBoxFlex( 'Parent', bottomPanel1, 'Spacing', 2);
                
                panelInputParams = uix.Panel( 'Parent', bottomPanel1HLayout);
                %panelMontageView = uix.Panel( 'Parent', bottomPanel1HLayout, 'Units','Normalized');
                panelMontageView = uipanel( 'Parent', bottomPanel1HLayout,'Units','Normalized');
                
                panelInputParamsVLayout = uix.VBoxFlex( 'Parent', panelInputParams, 'Spacing', 2);
                
                param1Layout = uix.HBoxFlex( 'Parent', panelInputParamsVLayout, 'Spacing', 2);
                uicontrol( 'Parent', param1Layout, 'style','text', 'String', 'Tpoplot Latencies','Units','Normalized');
                obj.gui.latencies = uicontrol( 'Parent', param1Layout, 'style', 'edit', 'Enable','on','Units','Normalized','String','[-100 0 100 200 300 400 500]');
                
                param2Layout = uix.HBoxFlex( 'Parent', panelInputParamsVLayout, 'Spacing', 2);
                uicontrol( 'Parent', param2Layout, 'style','text', 'String', 'ERP Image Channels','Units','Normalized');
                obj.gui.channels = uicontrol( 'Parent', param2Layout, 'style', 'edit', 'Enable','on','Units','Normalized','String','{''Fz'',''P4''}');
                
                param3Layout = uix.HBoxFlex( 'Parent', panelInputParamsVLayout, 'Spacing', 2);
                uicontrol( 'Parent', param3Layout, 'style','text', 'String', 'Bandpass frequencies','Units','Normalized');
                obj.gui.cutoffFreq = uicontrol( 'Parent', param3Layout, 'style', 'edit', 'Enable','on','Units','Normalized','String','[1 30]');
                
                param4Layout = uix.HBoxFlex( 'Parent', panelInputParamsVLayout, 'Spacing', 2);
                uicontrol( 'Parent', param4Layout, 'style','text', 'String', 'Source analysis','Units','Normalized');
                obj.gui.doSourceAnalysis = uicontrol( 'Parent', param4Layout, 'style', 'checkbox', 'Enable','on','Units','Normalized','Value',false);
                
                param5Layout = uix.HBoxFlex( 'Parent', panelInputParamsVLayout, 'Spacing', 2);
                panelInputParamsVLayout.Heights = -[1 1 1 1 10];
                param1Layout.Widths = -[1 1.7];
                param2Layout.Widths = -[1 1.7];
                param3Layout.Widths = -[1 1.7];
                param4Layout.Widths = -[1 1.7];
                
                obj.gui.ax = axes('Parent', panelMontageView, 'Units','Normalized');
                title(obj.gui.ax,'Montage')
                obj.gui.ax.YTickLabel = [];
                obj.gui.ax.XTickLabel = [];
                obj.gui.Window.Position(3:4) = [810 413];
                movegui(obj.gui.Window,'center');
                set(obj.gui.Window,'Visible','on');
                gui = obj.gui;
            catch ME
                disp(ME);
                disp('Please install the <a href="https://www.mathworks.com/matlabcentral/fileexchange/47982-gui-layout-toolbox">GUI Layout Toolbox</a>')
                close(obj.gui.Window);
            end
        end
        
        function loadData(obj, filename)
            [PathName,FileName,ex] = fileparts(filename);
            set(obj.gui.Window,'pointer','watch');
            drawnow;
            if strcmpi(ex,'.set')
                EEG = pop_loadset(filename);
            elseif strcmpi(ex,'.bdf') || strcmpi(ex,'.edf') || strcmpi(ex,'.gdf')
                EEG = pop_biosig(filename);
            elseif strcmpi(ex,'.xdf')
                EEG = pop_loadxdf(filename);
            else
                set(obj.gui.dataFile,'String','');
                set(obj.gui.reportFile,'String','');
                errordlg(['Cannot load file format ' ex]);
            end
            set(obj.gui.reportFile,'String',fullfile(PathName,[FileName '.' obj.reportType]));
            EEG = smarting2greentek(EEG);
            ind = find(ismember({EEG.chanlocs.labels},'TIMESTAMP'));
            if ~isempty(ind)
                EEG = pop_select(EEG,'nochannel',ind);
            end
            try
                EEG = pop_chanedit(EEG, 'eval','chans = pop_chancenter( chans, [],[]);');
            end
            template = which('head_modelColin27_5003_Standard-10-5-Cap339.mat');
            EEG = pop_forwardModel(EEG, template);
            hm = headModel.loadFromFile(EEG.etc.src.hmfile);
            cla(obj.gui.ax);
            hm.plotMontage(false, obj.gui.ax);
            set(obj.gui.Window,'pointer','arrow');
        end
    end
    
    methods(Static)
        function onSetDataFile(src, evnt)
            if ~exist(src.String,'file')
                src.String = '';
                return
            end
            obj = src.UserData;
            obj.loadData(src.String);
        end
        function onSelect(src, evnt)
            [FileName,PathName,FilterIndex] = uigetfile({'*.set' 'EEGLAB (.set)'; '*.bdf' 'Biosig (.bdf)';'*.xdf' 'LSL (.xdf)';'*.xdf' 'Smarting Greentek (.xdf)'},'Select file');
            if FilterIndex~=0
                filename = fullfile(PathName,FileName);
            else
                return;
            end
            obj = src.UserData;
            set(obj.gui.dataFile,'String',fullfile(PathName,FileName));
            set(obj.gui.reportFile,'String',fullfile(PathName,[FileName(1:end-3) obj.reportType]));
            obj.loadData(filename);
        end
        function onSave(src, evnt)
            obj = src.UserData;
            [FileName,PathName] = uiputfile(['*.' obj.reportType],'Select the report file');
            if isnumeric(FileName)
                return;
            end
            set(obj.gui.reportFile,'String', fullfile(PathName, FileName));
        end
        function onRun(src, evnt)
            obj = src.UserData;
            dataFile = obj.gui.dataFile.String;
            if ~exist(dataFile,'file')
                return;
            end
            reportFile = obj.gui.reportFile.String;
            latencies = eval(obj.gui.latencies.String);
            channels = eval(obj.gui.channels.String);
            cutoffFreq = eval(obj.gui.cutoffFreq.String);
            doSourceAnalysis = obj.gui.doSourceAnalysis.Value;
            
            obj.run(dataFile, reportFile, latencies, channels, cutoffFreq, doSourceAnalysis);
        end
        function onView(src, evnt)
            obj = src.UserData;
            if exist(obj.reportFile,'file')
                rptview(obj.reportFile);
            end
        end
        function onCloseAll(src, evnt)
            figs = findall(0,'Tag','BrainEAnalyzerFig');
            close(figs);
        end
        function onCancel(src, evnt)
            close(src.UserData.gui.Window);
        end
    end
end