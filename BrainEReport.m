classdef BrainEReport < ReportGenerator
    methods
        function obj = BrainEReport(reportTitle,reportType,reportFile)
            if nargin < 3
                reportFile = [];
            end
            obj = obj@ReportGenerator(reportTitle,reportType,reportFile);
        end
        function [reportFile, EEG] = generateReport(obj, dataFile, latencies, channels, cutoffFreq, doSourceAnalysis)
            [~,~,ex] = fileparts(dataFile);
            if strcmp(ex,'.set')
                EEG = pop_loadset(dataFile);
            elseif strcmp(ex,'.xdf')
                EEG = pop_loadxdf(dataFile);
            elseif strcmpi(ex,'.bdf') || strcmpi(ex,'.edf') || strcmpi(ex,'.gdf')
                EEG = pop_biosig(dataFile);
            end
            EEG = BrainEReport.preprocessing(EEG, cutoffFreq);
            obj.open;
            reportDir = fileparts(dataFile);
            f = dir(fullfile(fileparts(which('BrainEAnalyzer')),'+brainePkg'));
            assessments = {f.name};
            ind = ismember(assessments,{'.','..','BrainEAssessment.m'});
            assessments(ind) = [];
            for k=1:length(assessments), assessments{k}(end-1:end) = [];end
            for k=1:length(assessments)
                try
                    assessment_k = eval(['brainePkg.' assessments{k} '(latencies, channels, reportDir, doSourceAnalysis)']);
                    chap = assessment_k.generateReport(EEG);
                    add(obj.rpt, chap);
                catch ME
                    warning(['Assessment ' assessments{k} ' failed with the following exception: ' ]);
                    warning(ME.message)
                end
            end
            
%             middleFish = brainePkg.MiddleFish(latencies, channels, reportDir, doSourceAnalysis);
%             chap = middleFish.generateReport(EEG);
%             add(obj.rpt, chap);
%             
%             goGreen = brainePkg.GoGreen(latencies, channels, reportDir, doSourceAnalysis);
%             chap = goGreen.generateReport(EEG);
%             add(obj.rpt, chap);
%             
%             lostStart = brainePkg.LostStar(latencies, channels, reportDir, doSourceAnalysis);
%             chap = lostStart.generateReport(EEG);
%             add(obj.rpt, chap);
%             
%             faceOff = brainePkg.FaceOff(latencies, channels, reportDir, doSourceAnalysis);
%             chap = faceOff.generateReport(EEG);
%             add(obj.rpt, chap);
%             
%             twoTap = brainePkg.TwoTap(latencies, channels, reportDir, doSourceAnalysis);
%             chap = twoTap.generateReport(EEG);
%             add(obj.rpt, chap);
%             
%             rest = brainePkg.Rest(latencies, channels, reportDir, doSourceAnalysis);
%             chap = rest.generateReport(EEG);
%             add(obj.rpt, chap);
%             
%             luckyDoor = brainePkg.LuckyDoor(latencies, channels, reportDir, doSourceAnalysis);
%             chap = luckyDoor.generateReport(EEG);
%             add(obj.rpt, chap);
            
            obj.close;
            reportFile = obj.reportFile;
        end
    end
    methods(Static)
        function EEG = preprocessing(EEG, cutoffFreq)
            srate = 250;
            EEG.data = double(EEG.data);
            EEG = pop_resample( EEG, srate);
            EEG = pop_eegfiltnew(EEG, cutoffFreq(1), cutoffFreq(2), 826,0,[],0);
            EEG = pop_reref( EEG, []);
            EEG = smarting2greentek(EEG);
            ind = find(ismember({EEG.chanlocs.labels},'TIMESTAMP'));
            if ~isempty(ind)
                EEG = pop_select(EEG,'nochannel',ind);
            end
            try
                EEG = pop_chanedit(EEG, 'eval','chans = pop_chancenter( chans, [],[]);');
            end
            template = which('head_modelColin27_5003_Standard-10-5-Cap339-Destrieux148.mat');% which('head_modelColin27_5003_Standard-10-5-Cap339.mat');
            EEG = pop_forwardModel(EEG,template);
            xyz = [cell2mat({EEG.chanlocs.X})' cell2mat({EEG.chanlocs.Y})' cell2mat({EEG.chanlocs.Z})'];
            if isempty(xyz)
                hm = headModel.loadFromFile(EEG.etc.src.hmfile);
                EEG.chanlocs = hm.makeChanlocs;
                EEG = pop_chanedit(EEG, 'eval','chans = pop_chancenter( chans, [],[]);');
            end
        end
    end
end